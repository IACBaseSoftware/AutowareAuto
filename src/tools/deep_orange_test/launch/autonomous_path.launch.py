# Copyright 2020-2021 The Autoware Foundation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Testing of purepursuit_controller in LGSVL simulation using recordreplay planner."""

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from ament_index_python import get_package_share_directory
import os


def get_share_file(package_name, file_name):
    return os.path.join(get_package_share_directory(package_name), file_name)


def generate_launch_description():
    """
    Launch nodes with params to test record_replay_planner with pure pursuit in simulation.

    pure_pursuit_controller + LGSVL + recordreplay planner + lidar osbtacle ddetection
    """
    # --------------------------------- Params -------------------------------
    # pure_pursuit_controller_param_file = get_share_file(
    #     package_name='pure_pursuit_nodes',
    #     file_name='param/pure_pursuit.param.yaml')
    ssc_interface_param_file = get_share_file(
        package_name='ssc_interface', file_name='param/defaults.param.yaml')
    # pt_controller_param_file = get_share_file(
    #     package_name='pt_controller', file_name='param/___.param.yaml')
    # gnss_parma_file = get_share_file(
    #     package_name='gnss_package_name', file_name='param/___.param.yaml')
    # ros_can_launch_file = get_share_file(
    #     package_name='gnss_package_name', file_name='param/___.param.yaml')
    # replay_planner_param_file = get_share_file(
    #     package_name='replay_planner', file_name='param/defaults.param.yaml')
    

    # --------------------------------- Arguments -------------------------------
    # pure_pursuit_controller_param = DeclareLaunchArgument(
    #     'pure_pursuit_controller_param_file',
    #     default_value=pure_pursuit_controller_param_file,
    #     description='Path to config file for Pure Pursuit Controller'
    # )
    ssc_interface_param = DeclareLaunchArgument(
        'ssc_interface_param_file',
        default_value=ssc_interface_param_file,
        description='Path to config file for ssc_interface'
    )
    # pt_controller_param = DeclareLaunchArgument(
    #     'pt_controller_param_file',
    #     default_value=pt_controller_param_file,
    #     description='Path to config file for joystick translator'
    # )
    # gnss_param = DeclareLaunchArgument(
    #     'gnss_param_file',
    #     default_value=gnss_param_file,
    #     description='Path to config file for joystick translator'
    # )
    # ros_can_param = DeclareLaunchArgument(
    #     'ros_can_launch_file',
    #     default_value=ros_can_launch_file,
    #     description='Path to config file for joystick translator'
    # )
    # replay_planner_param = DeclareLaunchArgument(
    #     'replay_planner_param_file',
    #     default_value=replay_planner_param_file,
    #     description='Path to config file for joystick translator'
    # )
    
   

    # -------------------------------- Nodes-----------------------------------

    # pt_controller_node = Node(
    #     package="pt_controller",
    #     node_executable = "pt_controller",
    #     node_name ="pt_controller",
    #     output="screen",
    # )

    # pure_pursuit_controller_node = Node(
    #     package="pure_pursuit_nodes",
    #     node_executable="pure_pursuit_node_exe",
    #     node_name="pure_pursuit_controller",
    #     node_namespace='control',
    #     parameters=[LaunchConfiguration('pure_pursuit_controller_param_file')],
    #     output='screen',
    #     remappings=[
    #         ("current_pose", "/vehicle/vehicle_kinematic_state"),   #TODO: remember it
    #         ("trajectory", "/planning/trajectory"),
    #         ("ctrl_cmd", "/vehicle/vehicle_command"),
    #         ("ctrl_diag", "/control/control_diagnostic"),
    #     ],
    #     # condition=IfCondition(LaunchConfiguration('with_pure_pursuit'))
    # )

    diagnostics_node = Node(
        package="diagnostics",
        node_executable="emergency_diagnostics",
        node_name="emergency_diagnostics",
        output='screen'
    )


    ssc_interface_launch_file_path = get_share_file(
        package_name='ssc_interface',
        file_name='launch/ssc_interface.launch.py')
    ssc_interface = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(ssc_interface_launch_file_path),
        launch_arguments=[
            ("ssc_interface_param", LaunchConfiguration('ssc_interface_param_file'))]
    )

    # pt_controller_node = Node(
    #     package="pt_controller",
    #     node_executable="pt_controller",
    #     output='screen',
    # )
    # ros_can_launch_file_path = get_share_file(
    #     package_name='ros_can_package',
    #     file_name='launch/____.launch.py')
    # ros_can = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(joystick_launch_file_path),
    #     launch_arguments=[
    #         ("gnss_param", LaunchConfiguration('gnss_parma_file'))]
    # )

    # gnss_launch_file_path = get_share_file(
    #     package_name='gnss_package_name',
    #     file_name='launch/____.launch.py')
    # gnss = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(joystick_launch_file_path),
    #     launch_arguments=[
    #         ("gnss_param", LaunchConfiguration('gnss_parma_file'))]
    # )


 
    # replay_planner_path = get_share_file(
    #     package_name='replay_planner',
    #     file_name='launch/replay_planner_launch.launch.py')
    # replay_planner_node = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(replay_planner_path)
    # )
 
    return LaunchDescription([
        #pt_controller_node,
        # pure_pursuit_controller_param,
        # pure_pursuit_controller_node,
        ssc_interface_param,
        ssc_interface,
        # replay_planner_param,
        # replay_planner_node,         
        diagnostics_node
    ])
