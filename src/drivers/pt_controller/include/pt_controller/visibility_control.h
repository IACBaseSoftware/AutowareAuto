#ifndef PT_CONTROLLER__VISIBILITY_CONTROL_H_
#define PT_CONTROLLER__VISIBILITY_CONTROL_H_
#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define PT_CONTROLLER_EXPORT __attribute__ ((dllexport))
    #define PT_CONTROLLER_IMPORT __attribute__ ((dllimport))
  #else
    #define PT_CONTROLLER_EXPORT __declspec(dllexport)
    #define PT_CONTROLLER_IMPORT __declspec(dllimport)
  #endif
  #ifdef PT_CONTROLLER_BUILDING_LIBRARY
    #define PT_CONTROLLER_PUBLIC PT_CONTROLLER_EXPORT
  #else
    #define PT_CONTROLLER_PUBLIC PT_CONTROLLER_IMPORT
  #endif
  #define PT_CONTROLLER_PUBLIC_TYPE PT_CONTROLLER_PUBLIC
  #define PT_CONTROLLER_LOCAL
#else
  #define PT_CONTROLLER_EXPORT __attribute__ ((visibility("default")))
  #define PT_CONTROLLER_IMPORT
  #if __GNUC__ >= 4
    #define PT_CONTROLLER_PUBLIC __attribute__ ((visibility("default")))
    #define PT_CONTROLLER_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define PT_CONTROLLER_PUBLIC
    #define PT_CONTROLLER_LOCAL
  #endif
  #define PT_CONTROLLER_PUBLIC_TYPE
#endif
#endif  // PT_CONTROLLER__VISIBILITY_CONTROL_H_
// Generated 17-Apr-2021 00:04:24
 