/*
 * pt_controller_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "pt_controller".
 *
 * Model version              : 2.41
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Sat Apr 17 00:04:17 2021
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pt_controller_types_h_
#define RTW_HEADER_pt_controller_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Model Code Variants */
#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_Float32_
#define DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_Float32_

typedef struct {
  real32_T data;
} SL_Bus_std_msgs_Float32;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_UInt8_
#define DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_UInt8_

typedef struct {
  uint8_T data;
} SL_Bus_std_msgs_UInt8;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_builtin_interfaces_Time_
#define DEFINED_TYPEDEF_FOR_SL_Bus_builtin_interfaces_Time_

typedef struct {
  int32_T sec;
  uint32_T nanosec;
} SL_Bus_builtin_interfaces_Time;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_ROSVariableLengthArrayInfo_
#define DEFINED_TYPEDEF_FOR_SL_Bus_ROSVariableLengthArrayInfo_

typedef struct {
  uint32_T CurrentLength;
  uint32_T ReceivedLength;
} SL_Bus_ROSVariableLengthArrayInfo;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_Header_
#define DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_Header_

typedef struct {
  SL_Bus_builtin_interfaces_Time stamp;
  uint8_T frame_id[128];
  SL_Bus_ROSVariableLengthArrayInfo frame_id_SL_Info;
} SL_Bus_std_msgs_Header;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_builtin_interfaces_Duration_
#define DEFINED_TYPEDEF_FOR_SL_Bus_builtin_interfaces_Duration_

typedef struct {
  int32_T sec;
  uint32_T nanosec;
} SL_Bus_builtin_interfaces_Duration;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_autoware_auto_msgs_Complex32_
#define DEFINED_TYPEDEF_FOR_SL_Bus_autoware_auto_msgs_Complex32_

typedef struct {
  real32_T real;
  real32_T imag;
} SL_Bus_autoware_auto_msgs_Complex32;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_autoware_auto_msgs_TrajectoryPoint_
#define DEFINED_TYPEDEF_FOR_SL_Bus_autoware_auto_msgs_TrajectoryPoint_

typedef struct {
  SL_Bus_builtin_interfaces_Duration time_from_start;
  real32_T x;
  real32_T y;
  SL_Bus_autoware_auto_msgs_Complex32 heading;
  real32_T longitudinal_velocity_mps;
  real32_T lateral_velocity_mps;
  real32_T acceleration_mps2;
  real32_T heading_rate_rps;
  real32_T front_wheel_angle_rad;
  real32_T rear_wheel_angle_rad;
} SL_Bus_autoware_auto_msgs_TrajectoryPoint;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Vector3_
#define DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Vector3_

typedef struct {
  real_T x;
  real_T y;
  real_T z;
} SL_Bus_geometry_msgs_Vector3;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Quaternion_
#define DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Quaternion_

typedef struct {
  real_T x;
  real_T y;
  real_T z;
  real_T w;
} SL_Bus_geometry_msgs_Quaternion;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Transform_
#define DEFINED_TYPEDEF_FOR_SL_Bus_geometry_msgs_Transform_

typedef struct {
  SL_Bus_geometry_msgs_Vector3 translation;
  SL_Bus_geometry_msgs_Quaternion rotation;
} SL_Bus_geometry_msgs_Transform;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_autoware_auto_msgs_VehicleKinematicState_
#define DEFINED_TYPEDEF_FOR_SL_Bus_autoware_auto_msgs_VehicleKinematicState_

typedef struct {
  SL_Bus_std_msgs_Header header;
  SL_Bus_autoware_auto_msgs_TrajectoryPoint state;
  SL_Bus_geometry_msgs_Transform delta;
} SL_Bus_autoware_auto_msgs_VehicleKinematicState;

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_Float64_
#define DEFINED_TYPEDEF_FOR_SL_Bus_std_msgs_Float64_

typedef struct {
  real_T data;
} SL_Bus_std_msgs_Float64;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_GxrHM0sYOdie5URlXo3lH_
#define DEFINED_TYPEDEF_FOR_struct_GxrHM0sYOdie5URlXo3lH_

typedef struct {
  real_T ratio;
  real_T efficiency;
  real_T inertia;
} struct_GxrHM0sYOdie5URlXo3lH;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_YzRcaPNFqtxmfPoUDCrKyG_
#define DEFINED_TYPEDEF_FOR_struct_YzRcaPNFqtxmfPoUDCrKyG_

typedef struct {
  real_T ratio[6];
  real_T gear[6];
} struct_YzRcaPNFqtxmfPoUDCrKyG;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_5ByvgBbasQbyA349mhE0EE_
#define DEFINED_TYPEDEF_FOR_struct_5ByvgBbasQbyA349mhE0EE_

typedef struct {
  struct_YzRcaPNFqtxmfPoUDCrKyG gear_ratio;
  real_T efficiency;
  real_T inertia;
} struct_5ByvgBbasQbyA349mhE0EE;

#endif

/* Custom Type definition for MATLABSystem: '<S14>/SourceBlock' */
#include "rmw/qos_profiles.h"
#include "rmw/types.h"
#include "rmw/types.h"
#include "rmw/types.h"
#ifndef struct_tag_Qu7FNWL9Gkz0n0gKwUiDt
#define struct_tag_Qu7FNWL9Gkz0n0gKwUiDt

struct tag_Qu7FNWL9Gkz0n0gKwUiDt
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
};

#endif                                 /*struct_tag_Qu7FNWL9Gkz0n0gKwUiDt*/

#ifndef typedef_ros_slros2_internal_block_Pub_T
#define typedef_ros_slros2_internal_block_Pub_T

typedef tag_Qu7FNWL9Gkz0n0gKwUiDt ros_slros2_internal_block_Pub_T;

#endif                               /*typedef_ros_slros2_internal_block_Pub_T*/

#ifndef struct_tag_milCmLCmmRyhTcIYm0bVZB
#define struct_tag_milCmLCmmRyhTcIYm0bVZB

struct tag_milCmLCmmRyhTcIYm0bVZB
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
};

#endif                                 /*struct_tag_milCmLCmmRyhTcIYm0bVZB*/

#ifndef typedef_ros_slros2_internal_block_Sub_T
#define typedef_ros_slros2_internal_block_Sub_T

typedef tag_milCmLCmmRyhTcIYm0bVZB ros_slros2_internal_block_Sub_T;

#endif                               /*typedef_ros_slros2_internal_block_Sub_T*/

/* Parameters for system: '<S4>/Enabled Subsystem' */
typedef struct P_EnabledSubsystem_pt_control_T_ P_EnabledSubsystem_pt_control_T;

/* Parameters for system: '<S5>/Enabled Subsystem' */
typedef struct P_EnabledSubsystem_pt_contr_l_T_ P_EnabledSubsystem_pt_contr_l_T;

/* Parameters (default storage) */
typedef struct P_pt_controller_T_ P_pt_controller_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_pt_controller_T RT_MODEL_pt_controller_T;

#endif                                 /* RTW_HEADER_pt_controller_types_h_ */
