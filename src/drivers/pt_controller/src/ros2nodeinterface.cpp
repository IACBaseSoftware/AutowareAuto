// Copyright 2019-2021 The MathWorks, Inc.
// Generated 17-Apr-2021 00:04:21
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4244)
#pragma warning(disable : 4265)
#pragma warning(disable : 4458)
#pragma warning(disable : 4100)
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#pragma GCC diagnostic ignored "-Wdelete-non-virtual-dtor"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wshadow"
#endif //_MSC_VER
#include "rclcpp/rclcpp.hpp"
#include "pt_controller.h"
#include "ros2nodeinterface.h"
#include <thread>
#include <chrono>
#include <utility>
const std::string SLROSNodeName("pt_controller");
namespace ros2 {
namespace matlab {
NodeInterface::NodeInterface()
    : mNode()
    , mModel()
    , mExec()
    , mBaseRateSem()
    , mBaseRateThread()
    , mSchedulerThread()
    , mStopSem()
    , mRunModel(true){
  }
NodeInterface::~NodeInterface() {
    terminate();
  }
void NodeInterface::initialize(int argc, char * const argv[]) {
    try {
        //initialize ros2
        // Workaround to disable /rosout topic until rmw_fastrtps supports
        // multiple typesupport implementations for the same topic (https://github.com/ros2/rmw_fastrtps/issues/265)
        std::vector<char *> args(argv, argv + argc);
        char log_disable_rosout[] = "__log_disable_rosout:=true";
        args.push_back(log_disable_rosout);
        rclcpp::init(static_cast<int>(args.size()), args.data());
        //create the Node specified in Model
        std::string NodeName("pt_controller");
        mNode = std::make_shared<rclcpp::Node>(NodeName);
        mExec = std::make_shared<rclcpp::executors::MultiThreadedExecutor>();
        mExec->add_node(mNode);
        //initialize the model which will initialize the publishers and subscribers
        mModel = std::make_shared<pt_controllerModelClass>();
		rtmSetErrorStatus(mModel->getRTM(), (NULL));
        mModel->initialize();
        //create the threads for the rates in the Model
        mBaseRateThread = std::make_shared<std::thread>(&NodeInterface::baseRateTask, this);
		mSchedulerThread = std::make_shared<std::thread>(&NodeInterface::schedulerThreadCallback, this);
    }
    catch (std::exception& ex) {
        std::cout << ex.what() << std::endl;
        throw ex;
    }
    catch (...) {
        std::cout << "Unknown exception" << std::endl;
        throw;
    }
}
int NodeInterface::run() {
  if (mExec) {
    mExec->spin();
  }
  mRunModel = false;
  return 0;
}
boolean_T NodeInterface::getStopRequestedFlag(void) {
    #ifndef rtmGetStopRequested
    return (!(rtmGetErrorStatus(mModel->getRTM())
        == (NULL)));
    #else
    return (!(rtmGetErrorStatus(mModel->getRTM())
        == (NULL)) || rtmGetStopRequested(mModel->getRTM()));
    #endif
}
void NodeInterface::stop(void) {
  if (mExec.get()) {
    mExec->cancel();
    if (mNode) {
      mExec->remove_node(mNode);
    }
    while (mExec.use_count() > 1);
  }
}
void NodeInterface::terminate(void) {
    if (mBaseRateThread.get()) {
        mRunModel = false;
        mBaseRateSem.notify(); // break out wait
        mBaseRateThread->join();
        if (mSchedulerThread.get()) {
            mSchedulerThread->join();
            mSchedulerThread.reset();
        }
        mBaseRateThread.reset();
        if (mModel.get()) {
            mModel->terminate();
        }
        // Release publisher pt_controller/Raptor/Publish
        mPub_pt_controller_1693.reset();
        // Release publisher pt_controller/Raptor/Publish1
        mPub_pt_controller_1694.reset();
        // Release publisher pt_controller/Raptor/Publish2
        mPub_pt_controller_1955.reset();
        // Release subscriber pt_controller/Subscribe
        mSub_pt_controller_1695.reset();
        // Release subscriber pt_controller/Subscribe1
        mSub_pt_controller_1696.reset();
        // Release subscriber pt_controller/Subscribe10
        mSub_pt_controller_2594.reset();
        // Release subscriber pt_controller/Subscribe11
        mSub_pt_controller_2595.reset();
        // Release subscriber pt_controller/Subscribe12
        mSub_pt_controller_2596.reset();
        // Release subscriber pt_controller/Subscribe13
        mSub_pt_controller_2597.reset();
        // Release subscriber pt_controller/Subscribe3
        mSub_pt_controller_1698.reset();
        // Release subscriber pt_controller/Subscribe5
        mSub_pt_controller_2589.reset();
        // Release subscriber pt_controller/Subscribe6
        mSub_pt_controller_2590.reset();
        // Release subscriber pt_controller/Subscribe7
        mSub_pt_controller_2591.reset();
        // Release subscriber pt_controller/Subscribe8
        mSub_pt_controller_2592.reset();
        // Release subscriber pt_controller/Subscribe9
        mSub_pt_controller_2593.reset();
        mModel.reset();
        mExec.reset();
        mNode.reset();
        rclcpp::shutdown();
    }
}
//
void NodeInterface::schedulerThreadCallback(void)
{
  while (mRunModel) {
        std::this_thread::sleep_until(std::chrono::system_clock::now() + std::chrono::nanoseconds(10000000));
        mBaseRateSem.notify();
    }
}
//Model specific
void NodeInterface::baseRateTask(void) {
  mRunModel = (rtmGetErrorStatus(mModel->getRTM()) ==
              (NULL));
  while (mRunModel) {
    mBaseRateSem.wait();
    if (!mRunModel) break;
    mModel->step();
    mRunModel &= !NodeInterface::getStopRequestedFlag(); //If RunModel and not stop requested
  }
  NodeInterface::stop();
}
// pt_controller/Raptor/Publish
void NodeInterface::create_Pub_pt_controller_1693(const char *topicName, const rmw_qos_profile_t& qosProfile){
  mPub_pt_controller_1693 = mNode->create_publisher<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile));
}
void NodeInterface::publish_Pub_pt_controller_1693(const SL_Bus_std_msgs_Float32* inBus) {
  auto msg = std::make_unique<std_msgs::msg::Float32>();
  convertFromBus(*msg, inBus);
  mPub_pt_controller_1693->publish(std::move(msg));
}
// pt_controller/Raptor/Publish1
void NodeInterface::create_Pub_pt_controller_1694(const char *topicName, const rmw_qos_profile_t& qosProfile){
  mPub_pt_controller_1694 = mNode->create_publisher<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile));
}
void NodeInterface::publish_Pub_pt_controller_1694(const SL_Bus_std_msgs_Float32* inBus) {
  auto msg = std::make_unique<std_msgs::msg::Float32>();
  convertFromBus(*msg, inBus);
  mPub_pt_controller_1694->publish(std::move(msg));
}
// pt_controller/Raptor/Publish2
void NodeInterface::create_Pub_pt_controller_1955(const char *topicName, const rmw_qos_profile_t& qosProfile){
  mPub_pt_controller_1955 = mNode->create_publisher<std_msgs::msg::UInt8>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile));
}
void NodeInterface::publish_Pub_pt_controller_1955(const SL_Bus_std_msgs_UInt8* inBus) {
  auto msg = std::make_unique<std_msgs::msg::UInt8>();
  convertFromBus(*msg, inBus);
  mPub_pt_controller_1955->publish(std::move(msg));
}
// pt_controller/Subscribe
void NodeInterface::create_Sub_pt_controller_1695(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](autoware_auto_msgs::msg::VehicleKinematicState::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_1695);
        mLatestMsg_Sub_pt_controller_1695 = msg;
    };
    mSub_pt_controller_1695 = mNode->create_subscription<autoware_auto_msgs::msg::VehicleKinematicState>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_1695(SL_Bus_autoware_auto_msgs_VehicleKinematicState* outBus) {
    if (mLatestMsg_Sub_pt_controller_1695.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_1695);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_1695);
        return true;
    }
    return false;
}
// pt_controller/Subscribe1
void NodeInterface::create_Sub_pt_controller_1696(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float64::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_1696);
        mLatestMsg_Sub_pt_controller_1696 = msg;
    };
    mSub_pt_controller_1696 = mNode->create_subscription<std_msgs::msg::Float64>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_1696(SL_Bus_std_msgs_Float64* outBus) {
    if (mLatestMsg_Sub_pt_controller_1696.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_1696);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_1696);
        return true;
    }
    return false;
}
// pt_controller/Subscribe10
void NodeInterface::create_Sub_pt_controller_2594(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2594);
        mLatestMsg_Sub_pt_controller_2594 = msg;
    };
    mSub_pt_controller_2594 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2594(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2594.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2594);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2594);
        return true;
    }
    return false;
}
// pt_controller/Subscribe11
void NodeInterface::create_Sub_pt_controller_2595(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2595);
        mLatestMsg_Sub_pt_controller_2595 = msg;
    };
    mSub_pt_controller_2595 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2595(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2595.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2595);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2595);
        return true;
    }
    return false;
}
// pt_controller/Subscribe12
void NodeInterface::create_Sub_pt_controller_2596(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2596);
        mLatestMsg_Sub_pt_controller_2596 = msg;
    };
    mSub_pt_controller_2596 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2596(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2596.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2596);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2596);
        return true;
    }
    return false;
}
// pt_controller/Subscribe13
void NodeInterface::create_Sub_pt_controller_2597(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2597);
        mLatestMsg_Sub_pt_controller_2597 = msg;
    };
    mSub_pt_controller_2597 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2597(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2597.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2597);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2597);
        return true;
    }
    return false;
}
// pt_controller/Subscribe3
void NodeInterface::create_Sub_pt_controller_1698(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float64::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_1698);
        mLatestMsg_Sub_pt_controller_1698 = msg;
    };
    mSub_pt_controller_1698 = mNode->create_subscription<std_msgs::msg::Float64>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_1698(SL_Bus_std_msgs_Float64* outBus) {
    if (mLatestMsg_Sub_pt_controller_1698.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_1698);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_1698);
        return true;
    }
    return false;
}
// pt_controller/Subscribe5
void NodeInterface::create_Sub_pt_controller_2589(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2589);
        mLatestMsg_Sub_pt_controller_2589 = msg;
    };
    mSub_pt_controller_2589 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2589(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2589.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2589);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2589);
        return true;
    }
    return false;
}
// pt_controller/Subscribe6
void NodeInterface::create_Sub_pt_controller_2590(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2590);
        mLatestMsg_Sub_pt_controller_2590 = msg;
    };
    mSub_pt_controller_2590 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2590(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2590.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2590);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2590);
        return true;
    }
    return false;
}
// pt_controller/Subscribe7
void NodeInterface::create_Sub_pt_controller_2591(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2591);
        mLatestMsg_Sub_pt_controller_2591 = msg;
    };
    mSub_pt_controller_2591 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2591(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2591.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2591);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2591);
        return true;
    }
    return false;
}
// pt_controller/Subscribe8
void NodeInterface::create_Sub_pt_controller_2592(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2592);
        mLatestMsg_Sub_pt_controller_2592 = msg;
    };
    mSub_pt_controller_2592 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2592(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2592.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2592);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2592);
        return true;
    }
    return false;
}
// pt_controller/Subscribe9
void NodeInterface::create_Sub_pt_controller_2593(const char *topicName, const rmw_qos_profile_t& qosProfile){
    auto callback = [this](std_msgs::msg::Float32::SharedPtr msg) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2593);
        mLatestMsg_Sub_pt_controller_2593 = msg;
    };
    mSub_pt_controller_2593 = mNode->create_subscription<std_msgs::msg::Float32>(topicName, ros2::matlab::getQOSSettingsFromRMW(qosProfile), callback);
}
bool NodeInterface::getLatestMessage_Sub_pt_controller_2593(SL_Bus_std_msgs_Float32* outBus) {
    if (mLatestMsg_Sub_pt_controller_2593.get()) {
        std::lock_guard<std::mutex> lockMsg(mtx_Sub_pt_controller_2593);
        convertToBus(outBus, *mLatestMsg_Sub_pt_controller_2593);
        return true;
    }
    return false;
}
// Helper for pt_controller/Raptor/Publish
void create_Pub_pt_controller_1693(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Pub_pt_controller_1693(topicName, qosProfile);
}
void publish_Pub_pt_controller_1693(const SL_Bus_std_msgs_Float32* inBus) {
  ros2::matlab::getNodeInterface()->publish_Pub_pt_controller_1693(inBus);
}
// Helper for pt_controller/Raptor/Publish1
void create_Pub_pt_controller_1694(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Pub_pt_controller_1694(topicName, qosProfile);
}
void publish_Pub_pt_controller_1694(const SL_Bus_std_msgs_Float32* inBus) {
  ros2::matlab::getNodeInterface()->publish_Pub_pt_controller_1694(inBus);
}
// Helper for pt_controller/Raptor/Publish2
void create_Pub_pt_controller_1955(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Pub_pt_controller_1955(topicName, qosProfile);
}
void publish_Pub_pt_controller_1955(const SL_Bus_std_msgs_UInt8* inBus) {
  ros2::matlab::getNodeInterface()->publish_Pub_pt_controller_1955(inBus);
}
// Helper for pt_controller/Subscribe
void create_Sub_pt_controller_1695(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_1695(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_1695(SL_Bus_autoware_auto_msgs_VehicleKinematicState* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_1695(outBus);
}
// Helper for pt_controller/Subscribe1
void create_Sub_pt_controller_1696(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_1696(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_1696(SL_Bus_std_msgs_Float64* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_1696(outBus);
}
// Helper for pt_controller/Subscribe10
void create_Sub_pt_controller_2594(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2594(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2594(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2594(outBus);
}
// Helper for pt_controller/Subscribe11
void create_Sub_pt_controller_2595(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2595(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2595(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2595(outBus);
}
// Helper for pt_controller/Subscribe12
void create_Sub_pt_controller_2596(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2596(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2596(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2596(outBus);
}
// Helper for pt_controller/Subscribe13
void create_Sub_pt_controller_2597(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2597(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2597(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2597(outBus);
}
// Helper for pt_controller/Subscribe3
void create_Sub_pt_controller_1698(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_1698(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_1698(SL_Bus_std_msgs_Float64* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_1698(outBus);
}
// Helper for pt_controller/Subscribe5
void create_Sub_pt_controller_2589(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2589(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2589(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2589(outBus);
}
// Helper for pt_controller/Subscribe6
void create_Sub_pt_controller_2590(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2590(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2590(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2590(outBus);
}
// Helper for pt_controller/Subscribe7
void create_Sub_pt_controller_2591(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2591(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2591(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2591(outBus);
}
// Helper for pt_controller/Subscribe8
void create_Sub_pt_controller_2592(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2592(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2592(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2592(outBus);
}
// Helper for pt_controller/Subscribe9
void create_Sub_pt_controller_2593(const char *topicName, const rmw_qos_profile_t& qosProfile){
  ros2::matlab::getNodeInterface()->create_Sub_pt_controller_2593(topicName, qosProfile);
}
bool getLatestMessage_Sub_pt_controller_2593(SL_Bus_std_msgs_Float32* outBus) {
  return ros2::matlab::getNodeInterface()->getLatestMessage_Sub_pt_controller_2593(outBus);
}
}//namespace matlab
}//namespace ros2
#ifdef _MSC_VER
#pragma warning(pop)
#else
#pragma GCC diagnostic pop
#endif //_MSC_VER
