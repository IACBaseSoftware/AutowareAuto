/*
 * pt_controller.cpp
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "pt_controller".
 *
 * Model version              : 2.41
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Sat Apr 17 00:04:17 2021
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "pt_controller.h"
#include "pt_controller_private.h"

/* Named constants for Chart: '<S1>/Transmission Shift Logic' */
const uint8_T pt_controlle_IN_NO_ACTIVE_CHILD = 0U;
const int32_T pt_controller_CALL_EVENT = -1;
const uint8_T pt_controller_IN_downshifting = 1U;
const uint8_T pt_controller_IN_fifth = 1U;
const uint8_T pt_controller_IN_first = 2U;
const uint8_T pt_controller_IN_fourth = 3U;
const uint8_T pt_controller_IN_second = 4U;
const uint8_T pt_controller_IN_sixth = 5U;
const uint8_T pt_controller_IN_steady_state = 2U;
const uint8_T pt_controller_IN_third = 6U;
const uint8_T pt_controller_IN_upshifting = 3U;
const int32_T pt_controller_event_DOWN = 0;
const int32_T pt_controller_event_UP = 1;
real_T look2_binlxpw(real_T u0, real_T u1, const real_T bp0[], const real_T bp1[],
                     const real_T table[], const uint32_T maxIndex[], uint32_T
                     stride)
{
  real_T fractions[2];
  real_T frac;
  real_T yL_0d0;
  real_T yL_0d1;
  uint32_T bpIndices[2];
  uint32_T bpIdx;
  uint32_T iLeft;
  uint32_T iRght;

  /* Column-major Lookup 2-D
     Search method: 'binary'
     Use previous index: 'off'
     Interpolation method: 'Linear point-slope'
     Extrapolation method: 'Linear'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  /* Prelookup - Index and Fraction
     Index Search method: 'binary'
     Extrapolation method: 'Linear'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  if (u0 <= bp0[0U]) {
    iLeft = 0U;
    frac = (u0 - bp0[0U]) / (bp0[1U] - bp0[0U]);
  } else if (u0 < bp0[maxIndex[0U]]) {
    /* Binary Search */
    bpIdx = maxIndex[0U] >> 1U;
    iLeft = 0U;
    iRght = maxIndex[0U];
    while (iRght - iLeft > 1U) {
      if (u0 < bp0[bpIdx]) {
        iRght = bpIdx;
      } else {
        iLeft = bpIdx;
      }

      bpIdx = (iRght + iLeft) >> 1U;
    }

    frac = (u0 - bp0[iLeft]) / (bp0[iLeft + 1U] - bp0[iLeft]);
  } else {
    iLeft = maxIndex[0U] - 1U;
    frac = (u0 - bp0[maxIndex[0U] - 1U]) / (bp0[maxIndex[0U]] - bp0[maxIndex[0U]
      - 1U]);
  }

  fractions[0U] = frac;
  bpIndices[0U] = iLeft;

  /* Prelookup - Index and Fraction
     Index Search method: 'binary'
     Extrapolation method: 'Linear'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'off'
     Remove protection against out-of-range input in generated code: 'off'
   */
  if (u1 <= bp1[0U]) {
    iLeft = 0U;
    frac = (u1 - bp1[0U]) / (bp1[1U] - bp1[0U]);
  } else if (u1 < bp1[maxIndex[1U]]) {
    /* Binary Search */
    bpIdx = maxIndex[1U] >> 1U;
    iLeft = 0U;
    iRght = maxIndex[1U];
    while (iRght - iLeft > 1U) {
      if (u1 < bp1[bpIdx]) {
        iRght = bpIdx;
      } else {
        iLeft = bpIdx;
      }

      bpIdx = (iRght + iLeft) >> 1U;
    }

    frac = (u1 - bp1[iLeft]) / (bp1[iLeft + 1U] - bp1[iLeft]);
  } else {
    iLeft = maxIndex[1U] - 1U;
    frac = (u1 - bp1[maxIndex[1U] - 1U]) / (bp1[maxIndex[1U]] - bp1[maxIndex[1U]
      - 1U]);
  }

  /* Column-major Interpolation 2-D
     Interpolation method: 'Linear point-slope'
     Use last breakpoint for index at or above upper limit: 'off'
     Overflow mode: 'portable wrapping'
   */
  bpIdx = iLeft * stride + bpIndices[0U];
  yL_0d0 = table[bpIdx];
  yL_0d0 += (table[bpIdx + 1U] - yL_0d0) * fractions[0U];
  bpIdx += stride;
  yL_0d1 = table[bpIdx];
  return (((table[bpIdx + 1U] - yL_0d1) * fractions[0U] + yL_0d1) - yL_0d0) *
    frac + yL_0d0;
}

uint32_T plook_u32d_binckan(real_T u, const real_T bp[], uint32_T maxIndex)
{
  uint32_T bpIndex;

  /* Prelookup - Index only
     Index Search method: 'binary'
     Interpolation method: 'Use nearest'
     Extrapolation method: 'Clip'
     Use previous index: 'off'
     Use last breakpoint for index at or above upper limit: 'on'
     Remove protection against out-of-range input in generated code: 'off'
   */
  if (u <= bp[0U]) {
    bpIndex = 0U;
  } else if (u < bp[maxIndex]) {
    bpIndex = binsearch_u32d(u, bp, maxIndex >> 1U, maxIndex);
    if ((bpIndex < maxIndex) && (bp[bpIndex + 1U] - u <= u - bp[bpIndex])) {
      bpIndex++;
    }
  } else {
    bpIndex = maxIndex;
  }

  return bpIndex;
}

uint32_T binsearch_u32d(real_T u, const real_T bp[], uint32_T startIndex,
  uint32_T maxIndex)
{
  uint32_T bpIdx;
  uint32_T bpIndex;
  uint32_T iRght;

  /* Binary Search */
  bpIdx = startIndex;
  bpIndex = 0U;
  iRght = maxIndex;
  while (iRght - bpIndex > 1U) {
    if (u < bp[bpIdx]) {
      iRght = bpIdx;
    } else {
      bpIndex = bpIdx;
    }

    bpIdx = (iRght + bpIndex) >> 1U;
  }

  return bpIndex;
}

/*
 * System initialize for enable system:
 *    '<S4>/Enabled Subsystem'
 *    '<S9>/Enabled Subsystem'
 */
void pt_controllerModelClass::pt_contro_EnabledSubsystem_Init
  (B_EnabledSubsystem_pt_control_T *localB, P_EnabledSubsystem_pt_control_T
   *localP)
{
  /* SystemInitialize for Outport: '<S94>/Out1' incorporates:
   *  Inport: '<S94>/In1'
   */
  localB->In1 = localP->Out1_Y0;
}

/*
 * Output and update for enable system:
 *    '<S4>/Enabled Subsystem'
 *    '<S9>/Enabled Subsystem'
 */
void pt_controllerModelClass::pt_controller_EnabledSubsystem(boolean_T
  rtu_Enable, const SL_Bus_std_msgs_Float64 *rtu_In1,
  B_EnabledSubsystem_pt_control_T *localB)
{
  /* Outputs for Enabled SubSystem: '<S4>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S94>/Enable'
   */
  if (rtu_Enable) {
    /* Inport: '<S94>/In1' */
    localB->In1 = *rtu_In1;
  }

  /* End of Outputs for SubSystem: '<S4>/Enabled Subsystem' */
}

/*
 * System initialize for enable system:
 *    '<S5>/Enabled Subsystem'
 *    '<S6>/Enabled Subsystem'
 *    '<S7>/Enabled Subsystem'
 *    '<S8>/Enabled Subsystem'
 *    '<S10>/Enabled Subsystem'
 *    '<S11>/Enabled Subsystem'
 *    '<S12>/Enabled Subsystem'
 *    '<S13>/Enabled Subsystem'
 *    '<S14>/Enabled Subsystem'
 */
void pt_controllerModelClass::pt_cont_EnabledSubsystem_m_Init
  (B_EnabledSubsystem_pt_contr_g_T *localB, P_EnabledSubsystem_pt_contr_l_T
   *localP)
{
  /* SystemInitialize for Outport: '<S95>/Out1' incorporates:
   *  Inport: '<S95>/In1'
   */
  localB->In1 = localP->Out1_Y0;
}

/*
 * Output and update for enable system:
 *    '<S5>/Enabled Subsystem'
 *    '<S6>/Enabled Subsystem'
 *    '<S7>/Enabled Subsystem'
 *    '<S8>/Enabled Subsystem'
 *    '<S10>/Enabled Subsystem'
 *    '<S11>/Enabled Subsystem'
 *    '<S12>/Enabled Subsystem'
 *    '<S13>/Enabled Subsystem'
 *    '<S14>/Enabled Subsystem'
 */
void pt_controllerModelClass::pt_controlle_EnabledSubsystem_n(boolean_T
  rtu_Enable, const SL_Bus_std_msgs_Float32 *rtu_In1,
  B_EnabledSubsystem_pt_contr_g_T *localB)
{
  /* Outputs for Enabled SubSystem: '<S5>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S95>/Enable'
   */
  if (rtu_Enable) {
    /* Inport: '<S95>/In1' */
    localB->In1 = *rtu_In1;
  }

  /* End of Outputs for SubSystem: '<S5>/Enabled Subsystem' */
}

void pt_controllerModelClass::SystemCore_setup_ctnkblhgrokw4
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[15] = { '/', 'a', 'c', 'c', '_', 'u', '_', 'l', 'i',
    'm', 'i', 't', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 15; i++) {
    pt_controller_B.b_zeroDelimTopic_f[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_f[15] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2592
    (&pt_controller_B.b_zeroDelimTopic_f[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_con_SystemCore_setup_ctnkblh
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[32] = { '/', 'v', 'e', 'l', 'o', 'c', 'i', 't', 'y',
    '_', 'd', 'e', 'm', 'a', 'n', 'd', '_', 'r', 'a', 't', 'e', '_', 'u', '_',
    'l', 'i', 'm', 'i', 't', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 32; i++) {
    pt_controller_B.b_zeroDelimTopic_c[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_c[32] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2596
    (&pt_controller_B.b_zeroDelimTopic_c[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_cont_SystemCore_setup_ctnkbl
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[20] = { '/', 'v', '_', 'd', 'e', 'm', 'a', 'n', 'd',
    '_', 'u', '_', 'l', 'i', 'm', 'i', 't', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 20; i++) {
    pt_controller_B.b_zeroDelimTopic_b[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_b[20] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2595
    (&pt_controller_B.b_zeroDelimTopic_b[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_contro_SystemCore_setup_ctnk
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[13] = { '/', 'v', 'e', 'l', 'o', 'c', 'i', 't', 'y',
    '_', 'r', 'e', 'f' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 13; i++) {
    pt_controller_B.b_zeroDelimTopic_pp[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_pp[13] = '\x00';
  ros2::matlab::create_Sub_pt_controller_1696
    (&pt_controller_B.b_zeroDelimTopic_pp[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_contr_SystemCore_setup_ctnkb
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[20] = { '/', 'v', '_', 'd', 'e', 'm', 'a', 'n', 'd',
    '_', 'l', '_', 'l', 'i', 'm', 'i', 't', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 20; i++) {
    pt_controller_B.b_zeroDelimTopic_cx[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_cx[20] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2594
    (&pt_controller_B.b_zeroDelimTopic_cx[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_co_SystemCore_setup_ctnkblhg
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[32] = { '/', 'v', 'e', 'l', 'o', 'c', 'i', 't', 'y',
    '_', 'd', 'e', 'm', 'a', 'n', 'd', '_', 'r', 'a', 't', 'e', '_', 'l', '_',
    'l', 'i', 'm', 'i', 't', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 32; i++) {
    pt_controller_B.b_zeroDelimTopic_m[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_m[32] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2597
    (&pt_controller_B.b_zeroDelimTopic_m[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_control_SystemCore_setup_ctn
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[32] = { '/', 'v', 'e', 'h', 'i', 'c', 'l', 'e', '/',
    'v', 'e', 'h', 'i', 'c', 'l', 'e', '_', 'k', 'i', 'n', 'e', 'm', 'a', 't',
    'i', 'c', '_', 's', 't', 'a', 't', 'e' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 32; i++) {
    pt_controller_B.b_zeroDelimTopic[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic[32] = '\x00';
  ros2::matlab::create_Sub_pt_controller_1695(&pt_controller_B.b_zeroDelimTopic
    [0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt__SystemCore_setup_ctnkblhgro
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[14] = { '/', 'p', '_', 'g', 'a', 'i', 'n', '_', 'a',
    'c', 'c', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 14; i++) {
    pt_controller_B.b_zeroDelimTopic_g1[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_g1[14] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2589
    (&pt_controller_B.b_zeroDelimTopic_g1[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_SystemCore_setup_ctnkblhgrok
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[14] = { '/', 'i', '_', 'g', 'a', 'i', 'n', '_', 'a',
    'c', 'c', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 14; i++) {
    pt_controller_B.b_zeroDelimTopic_g[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_g[14] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2590
    (&pt_controller_B.b_zeroDelimTopic_g[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::SystemCore_setup_ctnkblhgrokw4x
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[15] = { '/', 'a', 'c', 'c', '_', 'l', '_', 'l', 'i',
    'm', 'i', 't', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 15; i++) {
    pt_controller_B.b_zeroDelimTopic_cv[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_cv[15] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2593
    (&pt_controller_B.b_zeroDelimTopic_cv[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_controller_SystemCore_setup
  (ros_slros2_internal_block_Pub_T *obj)
{
  static const char_T tmp[13] = { '/', 'b', 'r', 'a', 'k', 'e', '_', 'c', 'm',
    'd', '_', 'p', 't' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 13; i++) {
    pt_controller_B.b_zeroDelimTopic_n[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_n[13] = '\x00';
  ros2::matlab::create_Pub_pt_controller_1693
    (&pt_controller_B.b_zeroDelimTopic_n[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_c_SystemCore_setup_ctnkblhgr
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[21] = { '/', 'g', 'e', 'a', 'r', '_', 'p', 'o', 's',
    'i', 't', 'i', 'o', 'n', '_', 'a', 'c', 't', 'u', 'a', 'l' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 21; i++) {
    pt_controller_B.b_zeroDelimTopic_k[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_k[21] = '\x00';
  ros2::matlab::create_Sub_pt_controller_1698
    (&pt_controller_B.b_zeroDelimTopic_k[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::p_SystemCore_setup_ctnkblhgrokw
  (ros_slros2_internal_block_Sub_T *obj)
{
  static const char_T tmp[13] = { '/', 'p', '_', 'g', 'a', 'i', 'n', '_', 'e',
    'g', '_', 'u', 'i' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 13; i++) {
    pt_controller_B.b_zeroDelimTopic_me[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_me[13] = '\x00';
  ros2::matlab::create_Sub_pt_controller_2591
    (&pt_controller_B.b_zeroDelimTopic_me[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_controlle_SystemCore_setup_c
  (ros_slros2_internal_block_Pub_T *obj)
{
  static const char_T tmp[19] = { '/', 'a', 'c', 'c', 'e', 'l', 'e', 'r', 'a',
    't', 'o', 'r', '_', 'c', 'm', 'd', '_', 'p', 't' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 19; i++) {
    pt_controller_B.b_zeroDelimTopic_p[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_p[19] = '\x00';
  ros2::matlab::create_Pub_pt_controller_1694
    (&pt_controller_B.b_zeroDelimTopic_p[0], qos_profile);
  obj->isSetupComplete = true;
}

void pt_controllerModelClass::pt_controll_SystemCore_setup_ct
  (ros_slros2_internal_block_Pub_T *obj)
{
  static const char_T tmp[12] = { '/', 'g', 'e', 'a', 'r', '_', 'c', 'm', 'd',
    '_', 'p', 't' };

  rmw_qos_durability_policy_t durability;
  rmw_qos_history_policy_t history;
  rmw_qos_profile_t qos_profile;
  rmw_qos_reliability_policy_t reliability;
  int32_T i;
  obj->isSetupComplete = false;
  obj->isInitialized = 1;
  qos_profile = rmw_qos_profile_default;
  history = RMW_QOS_POLICY_HISTORY_KEEP_LAST;
  reliability = RMW_QOS_POLICY_RELIABILITY_RELIABLE;
  durability = RMW_QOS_POLICY_DURABILITY_VOLATILE;
  SET_QOS_VALUES(qos_profile, history, (size_t)1.0, durability, reliability);
  for (i = 0; i < 12; i++) {
    pt_controller_B.b_zeroDelimTopic_l[i] = tmp[i];
  }

  pt_controller_B.b_zeroDelimTopic_l[12] = '\x00';
  ros2::matlab::create_Pub_pt_controller_1955
    (&pt_controller_B.b_zeroDelimTopic_l[0], qos_profile);
  obj->isSetupComplete = true;
}

/* Function for Chart: '<S1>/Transmission Shift Logic' */
void pt_controllerModelClass::pt_controller_gear_state(void)
{
  switch (pt_controller_DW.is_gear_state) {
   case pt_controller_IN_fifth:
    pt_controller_B.Gear = 5.0;
    if (pt_controller_DW.sfEvent == pt_controller_event_UP) {
      pt_controller_DW.is_gear_state = pt_controller_IN_sixth;
      pt_controller_B.Gear = 6.0;
    } else {
      if (pt_controller_DW.sfEvent == pt_controller_event_DOWN) {
        pt_controller_DW.is_gear_state = pt_controller_IN_fourth;
        pt_controller_B.Gear = 4.0;
      }
    }
    break;

   case pt_controller_IN_first:
    pt_controller_B.Gear = 1.0;
    if (pt_controller_DW.sfEvent == pt_controller_event_UP) {
      pt_controller_DW.is_gear_state = pt_controller_IN_second;
      pt_controller_B.Gear = 2.0;
    }
    break;

   case pt_controller_IN_fourth:
    pt_controller_B.Gear = 4.0;
    if (pt_controller_DW.sfEvent == pt_controller_event_UP) {
      pt_controller_DW.is_gear_state = pt_controller_IN_fifth;
      pt_controller_B.Gear = 5.0;
    } else {
      if (pt_controller_DW.sfEvent == pt_controller_event_DOWN) {
        pt_controller_DW.is_gear_state = pt_controller_IN_third;
        pt_controller_B.Gear = 3.0;
      }
    }
    break;

   case pt_controller_IN_second:
    pt_controller_B.Gear = 2.0;
    if (pt_controller_DW.sfEvent == pt_controller_event_UP) {
      pt_controller_DW.is_gear_state = pt_controller_IN_third;
      pt_controller_B.Gear = 3.0;
    } else {
      if (pt_controller_DW.sfEvent == pt_controller_event_DOWN) {
        pt_controller_DW.is_gear_state = pt_controller_IN_first;
        pt_controller_B.Gear = 1.0;
      }
    }
    break;

   case pt_controller_IN_sixth:
    pt_controller_B.Gear = 6.0;
    if (pt_controller_DW.sfEvent == pt_controller_event_DOWN) {
      pt_controller_DW.is_gear_state = pt_controller_IN_fifth;
      pt_controller_B.Gear = 5.0;
    }
    break;

   case pt_controller_IN_third:
    pt_controller_B.Gear = 3.0;
    if (pt_controller_DW.sfEvent == pt_controller_event_UP) {
      pt_controller_DW.is_gear_state = pt_controller_IN_fourth;
      pt_controller_B.Gear = 4.0;
    } else {
      if (pt_controller_DW.sfEvent == pt_controller_event_DOWN) {
        pt_controller_DW.is_gear_state = pt_controller_IN_second;
        pt_controller_B.Gear = 2.0;
      }
    }
    break;

   default:
    /* Unreachable state, for coverage only */
    pt_controller_DW.is_gear_state = pt_controlle_IN_NO_ACTIVE_CHILD;
    break;
  }
}

/* Model step function */
void pt_controllerModelClass::step()
{
  /* local block i/o variables */
  real_T rtb_i;
  real_T rtb_p;
  real_T rtb_ManualSwitch;
  SL_Bus_std_msgs_Float32 b_varargout_2;
  SL_Bus_std_msgs_Float32 rtb_BusAssignment;
  SL_Bus_std_msgs_Float32 rtb_BusAssignment1;
  SL_Bus_std_msgs_UInt8 rtb_BusAssignment2;
  real_T rtb_Gain_a_0;
  int32_T b_previousEvent;
  real32_T rtb_DataTypeConversion;
  boolean_T b_varargout_1;

  /* MATLABSystem: '<S13>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2592
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S13>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_d0);

  /* End of Outputs for SubSystem: '<S13>/Enabled Subsystem' */

  /* MATLABSystem: '<S7>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2596
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S7>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_j);

  /* End of Outputs for SubSystem: '<S7>/Enabled Subsystem' */

  /* Product: '<S17>/delta fall limit' incorporates:
   *  Product: '<S17>/delta rise limit'
   *  Product: '<S80>/Product'
   *  SampleTimeMath: '<S17>/sample time'
   *
   * About '<S17>/sample time':
   *  y = K where K = ( w * Ts )
   */
  rtb_p = pt_controller_B.EnabledSubsystem_j.In1.data *
    pt_controller_P.sampletime_WtEt;

  /* MATLABSystem: '<S6>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2595
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S6>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_l);

  /* End of Outputs for SubSystem: '<S6>/Enabled Subsystem' */

  /* MATLABSystem: '<S4>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_1696
    (&pt_controller_B.b_varargout_2_j);

  /* Outputs for Enabled SubSystem: '<S4>/Enabled Subsystem' */
  pt_controller_EnabledSubsystem(b_varargout_1, &pt_controller_B.b_varargout_2_j,
    &pt_controller_B.EnabledSubsystem_d);

  /* End of Outputs for SubSystem: '<S4>/Enabled Subsystem' */

  /* MATLABSystem: '<S5>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2594
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S5>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_n);

  /* End of Outputs for SubSystem: '<S5>/Enabled Subsystem' */

  /* Switch: '<S18>/Switch2' incorporates:
   *  RelationalOperator: '<S18>/LowerRelop1'
   *  RelationalOperator: '<S18>/UpperRelop'
   *  Switch: '<S18>/Switch'
   */
  if (pt_controller_B.EnabledSubsystem_d.In1.data >
      pt_controller_B.EnabledSubsystem_l.In1.data) {
    pt_controller_B.Switch1 = pt_controller_B.EnabledSubsystem_l.In1.data;
  } else if (pt_controller_B.EnabledSubsystem_d.In1.data <
             pt_controller_B.EnabledSubsystem_n.In1.data) {
    /* Switch: '<S18>/Switch' */
    pt_controller_B.Switch1 = pt_controller_B.EnabledSubsystem_n.In1.data;
  } else {
    pt_controller_B.Switch1 = pt_controller_B.EnabledSubsystem_d.In1.data;
  }

  /* End of Switch: '<S18>/Switch2' */

  /* Sum: '<S17>/Difference Inputs1' incorporates:
   *  UnitDelay: '<S17>/Delay Input2'
   */
  pt_controller_B.UkYk1 = pt_controller_B.Switch1 -
    pt_controller_DW.DelayInput2_DSTATE;

  /* MATLABSystem: '<S8>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2597
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S8>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_ni);

  /* End of Outputs for SubSystem: '<S8>/Enabled Subsystem' */

  /* Switch: '<S72>/Switch2' incorporates:
   *  RelationalOperator: '<S72>/LowerRelop1'
   */
  if (!(pt_controller_B.UkYk1 > rtb_p)) {
    /* Product: '<S17>/delta fall limit' incorporates:
     *  SampleTimeMath: '<S17>/sample time'
     *
     * About '<S17>/sample time':
     *  y = K where K = ( w * Ts )
     */
    rtb_p = pt_controller_B.EnabledSubsystem_ni.In1.data *
      pt_controller_P.sampletime_WtEt;

    /* Switch: '<S72>/Switch' incorporates:
     *  RelationalOperator: '<S72>/UpperRelop'
     */
    if (!(pt_controller_B.UkYk1 < rtb_p)) {
      /* Product: '<S17>/delta fall limit' incorporates:
       *  Product: '<S80>/Product'
       */
      rtb_p = pt_controller_B.UkYk1;
    }

    /* End of Switch: '<S72>/Switch' */
  }

  /* End of Switch: '<S72>/Switch2' */

  /* Sum: '<S17>/Difference Inputs2' incorporates:
   *  UnitDelay: '<S17>/Delay Input2'
   */
  pt_controller_DW.DelayInput2_DSTATE += rtb_p;

  /* MATLABSystem: '<S3>/SourceBlock' incorporates:
   *  Inport: '<S93>/In1'
   */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_1695
    (&pt_controller_B.b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S3>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S93>/Enable'
   */
  if (b_varargout_1) {
    pt_controller_B.In1 = pt_controller_B.b_varargout_2;
  }

  /* End of MATLABSystem: '<S3>/SourceBlock' */
  /* End of Outputs for SubSystem: '<S3>/Enabled Subsystem' */

  /* Sum: '<S1>/Minus' */
  pt_controller_B.UkYk1 = pt_controller_DW.DelayInput2_DSTATE -
    pt_controller_B.In1.state.longitudinal_velocity_mps;

  /* MATLABSystem: '<S10>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2589
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S10>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_dt);

  /* End of Outputs for SubSystem: '<S10>/Enabled Subsystem' */

  /* Product: '<S17>/delta fall limit' incorporates:
   *  Product: '<S80>/Product'
   */
  rtb_p = pt_controller_B.UkYk1 * pt_controller_B.EnabledSubsystem_dt.In1.data;

  /* Sum: '<S79>/Add4' incorporates:
   *  Delay: '<S80>/Delay'
   *  ManualSwitch: '<S22>/Manual Switch'
   */
  rtb_ManualSwitch = pt_controller_DW.Delay_DSTATE;

  /* MATLABSystem: '<S11>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2590
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S11>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_k);

  /* End of Outputs for SubSystem: '<S11>/Enabled Subsystem' */

  /* Sum: '<S80>/Add3' incorporates:
   *  Constant: '<S80>/dT'
   *  Product: '<S80>/Product1'
   *  UnitDelay: '<S80>/Unit Delay1'
   */
  rtb_i = rtb_ManualSwitch * pt_controller_B.EnabledSubsystem_k.In1.data *
    pt_controller_P.dt_controller + pt_controller_DW.UnitDelay1_DSTATE;

  /* Saturate: '<S80>/Saturation' */
  if (rtb_i > pt_controller_P.Saturation_UpperSat) {
    /* Sum: '<S80>/Add3' incorporates:
     *  Saturate: '<S80>/Saturation'
     */
    rtb_i = pt_controller_P.Saturation_UpperSat;
  } else {
    if (rtb_i < pt_controller_P.Saturation_LowerSat) {
      /* Sum: '<S80>/Add3' incorporates:
       *  Saturate: '<S80>/Saturation'
       */
      rtb_i = pt_controller_P.Saturation_LowerSat;
    }
  }

  /* End of Saturate: '<S80>/Saturation' */

  /* Sum: '<S80>/Add2' incorporates:
   *  UnitDelay: '<S80>/Unit Delay'
   */
  pt_controller_DW.UnitDelay_DSTATE = pt_controller_B.UkYk1 -
    pt_controller_DW.UnitDelay_DSTATE;

  /* MultiPortSwitch: '<S80>/Multiport Switch' incorporates:
   *  Constant: '<S80>/Constant2'
   *  Constant: '<S80>/d Gain2'
   *  Constant: '<S80>/dT'
   *  Product: '<S80>/Product2'
   *  UnitDelay: '<S80>/Unit Delay2'
   */
  if (!pt_controller_DW.UnitDelay2_DSTATE) {
    pt_controller_B.Switch2_o = pt_controller_DW.UnitDelay_DSTATE /
      pt_controller_P.dt_controller * pt_controller_P.dGain2_Value;
  } else {
    pt_controller_B.Switch2_o = pt_controller_P.Constant2_Value;
  }

  /* End of MultiPortSwitch: '<S80>/Multiport Switch' */

  /* Sum: '<S80>/Add4' */
  pt_controller_B.Add4 = (rtb_p + rtb_i) + pt_controller_B.Switch2_o;

  /* MATLABSystem: '<S14>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2593
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S14>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_g);

  /* End of Outputs for SubSystem: '<S14>/Enabled Subsystem' */

  /* Switch: '<S84>/Switch2' incorporates:
   *  RelationalOperator: '<S84>/LowerRelop1'
   *  RelationalOperator: '<S84>/UpperRelop'
   *  Switch: '<S84>/Switch'
   */
  if (pt_controller_B.Add4 > pt_controller_B.EnabledSubsystem_d0.In1.data) {
    pt_controller_B.Switch2_o = pt_controller_B.EnabledSubsystem_d0.In1.data;
  } else if (pt_controller_B.Add4 < pt_controller_B.EnabledSubsystem_g.In1.data)
  {
    /* Switch: '<S84>/Switch' */
    pt_controller_B.Switch2_o = pt_controller_B.EnabledSubsystem_g.In1.data;
  } else {
    pt_controller_B.Switch2_o = pt_controller_B.Add4;
  }

  /* End of Switch: '<S84>/Switch2' */

  /* Switch: '<S81>/Switch1' incorporates:
   *  Constant: '<S81>/I Gain1'
   */
  if (pt_controller_B.In1.state.longitudinal_velocity_mps >
      pt_controller_P.Switch1_Threshold_j) {
    /* Switch: '<S81>/Switch2' incorporates:
     *  Constant: '<S81>/I Gain2'
     *  Constant: '<S81>/I Gain3'
     */
    if (pt_controller_B.In1.state.longitudinal_velocity_mps >
        pt_controller_P.Switch2_Threshold) {
      pt_controller_B.Switch1 = pt_controller_P.IGain3_Value;
    } else {
      pt_controller_B.Switch1 = pt_controller_P.IGain2_Value;
    }

    /* End of Switch: '<S81>/Switch2' */
  } else {
    pt_controller_B.Switch1 = pt_controller_P.IGain1_Value;
  }

  /* End of Switch: '<S81>/Switch1' */

  /* Switch: '<S82>/Switch' */
  if (pt_controller_B.In1.state.longitudinal_velocity_mps >
      pt_controller_P.Switch_Threshold_b) {
    /* Switch: '<S82>/Switch1' */
    if (pt_controller_B.In1.state.longitudinal_velocity_mps >
        pt_controller_P.Switch1_Threshold) {
      /* Sum: '<S79>/Add4' incorporates:
       *  Constant: '<S82>/I Gain3'
       *  ManualSwitch: '<S22>/Manual Switch'
       */
      rtb_ManualSwitch = pt_controller_P.IGain3_Value_d;
    } else {
      /* Sum: '<S79>/Add4' incorporates:
       *  Constant: '<S82>/I Gain1'
       *  ManualSwitch: '<S22>/Manual Switch'
       */
      rtb_ManualSwitch = pt_controller_P.IGain1_Value_d;
    }

    /* End of Switch: '<S82>/Switch1' */
  } else {
    /* Sum: '<S79>/Add4' incorporates:
     *  Constant: '<S82>/I Gain2'
     *  ManualSwitch: '<S22>/Manual Switch'
     */
    rtb_ManualSwitch = pt_controller_P.IGain2_Value_g;
  }

  /* End of Switch: '<S82>/Switch' */

  /* Sum: '<S79>/Add3' incorporates:
   *  Constant: '<S79>/dT'
   *  Product: '<S79>/Product1'
   *  UnitDelay: '<S79>/Unit Delay1'
   */
  pt_controller_DW.UnitDelay1_DSTATE_d += pt_controller_B.UkYk1 *
    rtb_ManualSwitch * pt_controller_P.dT_Value;

  /* Switch: '<S83>/Switch2' incorporates:
   *  RelationalOperator: '<S83>/LowerRelop1'
   */
  if (pt_controller_DW.UnitDelay1_DSTATE_d > pt_controller_B.Switch1) {
    /* Sum: '<S79>/Add3' */
    pt_controller_DW.UnitDelay1_DSTATE_d = pt_controller_B.Switch1;
  } else {
    /* Gain: '<S81>/Gain' */
    pt_controller_B.Switch1 *= pt_controller_P.Gain_Gain;

    /* Switch: '<S83>/Switch' incorporates:
     *  RelationalOperator: '<S83>/UpperRelop'
     */
    if (pt_controller_DW.UnitDelay1_DSTATE_d < pt_controller_B.Switch1) {
      /* Sum: '<S79>/Add3' */
      pt_controller_DW.UnitDelay1_DSTATE_d = pt_controller_B.Switch1;
    }

    /* End of Switch: '<S83>/Switch' */
  }

  /* End of Switch: '<S83>/Switch2' */

  /* Sum: '<S79>/Add2' incorporates:
   *  UnitDelay: '<S79>/Unit Delay'
   */
  pt_controller_DW.UnitDelay_DSTATE_e = pt_controller_B.UkYk1 -
    pt_controller_DW.UnitDelay_DSTATE_e;

  /* ManualSwitch: '<S22>/Manual Switch' incorporates:
   *  MultiPortSwitch: '<S79>/Multiport Switch'
   *  UnitDelay: '<S79>/Unit Delay2'
   */
  if (pt_controller_P.ManualSwitch_CurrentSetting == 1) {
    /* Sum: '<S79>/Add4' incorporates:
     *  ManualSwitch: '<S22>/Manual Switch'
     */
    rtb_ManualSwitch = pt_controller_B.Switch2_o;
  } else {
    if (!pt_controller_DW.UnitDelay2_DSTATE_c) {
      /* Sum: '<S79>/Add4' incorporates:
       *  Constant: '<S79>/d Gain2'
       *  Constant: '<S79>/dT'
       *  MultiPortSwitch: '<S79>/Multiport Switch'
       *  Product: '<S79>/Product2'
       */
      rtb_ManualSwitch = pt_controller_DW.UnitDelay_DSTATE_e /
        pt_controller_P.dT_Value * pt_controller_P.dGain2_Value_b;
    } else {
      /* Sum: '<S79>/Add4' incorporates:
       *  Constant: '<S79>/Constant2'
       *  MultiPortSwitch: '<S79>/Multiport Switch'
       */
      rtb_ManualSwitch = pt_controller_P.Constant2_Value_m;
    }

    /* Sum: '<S79>/Add4' incorporates:
     *  Constant: '<S79>/P Gain'
     *  Product: '<S79>/Product'
     */
    rtb_ManualSwitch += pt_controller_B.UkYk1 * pt_controller_P.PGain_Value +
      pt_controller_DW.UnitDelay1_DSTATE_d;

    /* Saturate: '<S79>/Saturation1' */
    if (rtb_ManualSwitch > pt_controller_P.Saturation1_UpperSat) {
      /* Sum: '<S79>/Add4' incorporates:
       *  ManualSwitch: '<S22>/Manual Switch'
       */
      rtb_ManualSwitch = pt_controller_P.Saturation1_UpperSat;
    } else {
      if (rtb_ManualSwitch < pt_controller_P.Saturation1_LowerSat) {
        /* Sum: '<S79>/Add4' incorporates:
         *  ManualSwitch: '<S22>/Manual Switch'
         */
        rtb_ManualSwitch = pt_controller_P.Saturation1_LowerSat;
      }
    }

    /* End of Saturate: '<S79>/Saturation1' */
  }

  /* End of ManualSwitch: '<S22>/Manual Switch' */

  /* Math: '<S78>/Square' */
  rtb_DataTypeConversion = pt_controller_B.In1.state.longitudinal_velocity_mps *
    pt_controller_B.In1.state.longitudinal_velocity_mps;

  /* Sum: '<S20>/Total Road load' incorporates:
   *  Constant: '<S75>/Constant15'
   *  Constant: '<S76>/Constant7'
   *  Constant: '<S77>/Cr'
   *  Constant: '<S78>/Constant1'
   *  Constant: '<S78>/Constant10'
   *  Constant: '<S78>/Constant8'
   *  Constant: '<S78>/Constant9'
   *  Product: '<S75>/Downf'
   *  Product: '<S76>/inertial f'
   *  Product: '<S77>/Roll f'
   *  Product: '<S78>/Aero Force '
   */
  pt_controller_B.PProdOut = (pt_controller_P.Constant8_Value *
    pt_controller_P.Cd * pt_controller_P.rho * pt_controller_P.Constant1_Value *
    rtb_DataTypeConversion + rtb_ManualSwitch * pt_controller_P.m) +
    pt_controller_P.Constant8_Value * pt_controller_P.rho *
    pt_controller_P.Constant1_Value * rtb_DataTypeConversion *
    pt_controller_P.Cl * pt_controller_P.Cr_Value;

  /* Product: '<S20>/Demand Torque at wheels' incorporates:
   *  Constant: '<S20>/Constant'
   */
  pt_controller_B.Switch1 = pt_controller_B.PProdOut * pt_controller_P.Re_rr;

  /* Switch: '<S1>/Switch' */
  if (!(pt_controller_B.Switch1 >= pt_controller_P.Switch_Threshold)) {
    pt_controller_B.Switch1 = pt_controller_B.PProdOut;
  }

  /* End of Switch: '<S1>/Switch' */

  /* Gain: '<S74>/Gain' incorporates:
   *  Constant: '<S74>/Caliper pad bore'
   */
  pt_controller_B.PProdOut = pt_controller_P.Gain_Gain_a *
    pt_controller_P.Bore_BCF;

  /* Saturate: '<S1>/Negative Force Demand' */
  if (pt_controller_B.Switch1 > pt_controller_P.NegativeForceDemand_UpperSat) {
    rtb_Gain_a_0 = pt_controller_P.NegativeForceDemand_UpperSat;
  } else if (pt_controller_B.Switch1 <
             pt_controller_P.NegativeForceDemand_LowerSat) {
    rtb_Gain_a_0 = pt_controller_P.NegativeForceDemand_LowerSat;
  } else {
    rtb_Gain_a_0 = pt_controller_B.Switch1;
  }

  /* End of Saturate: '<S1>/Negative Force Demand' */

  /* Product: '<S74>/Divide' incorporates:
   *  Constant: '<S1>/Constant'
   *  Constant: '<S73>/Constant'
   *  Constant: '<S74>/Constant'
   *  Gain: '<S19>/Gain'
   *  Gain: '<S21>/Gain1'
   *  Gain: '<S73>/Gain'
   *  Gain: '<S73>/Gain2'
   *  Math: '<S74>/Square'
   *  Product: '<S21>/Product'
   *  Product: '<S73>/Divide3'
   *  Product: '<S74>/Product'
   */
  pt_controller_B.PProdOut = rtb_Gain_a_0 * pt_controller_P.bias *
    pt_controller_P.Gain1_Gain * pt_controller_P.Gain_Gain_j *
    pt_controller_P.Re_fl / (pt_controller_P.Rm_F *
    pt_controller_P.Constant_Value_hd) / (pt_controller_B.PProdOut *
    pt_controller_B.PProdOut * pt_controller_P.Constant_Value_c);

  /* Saturate: '<S1>/Saturation' */
  if (pt_controller_B.PProdOut > pt_controller_P.Saturation_UpperSat_l) {
    pt_controller_B.PProdOut = pt_controller_P.Saturation_UpperSat_l;
  } else {
    if (pt_controller_B.PProdOut < pt_controller_P.Saturation_LowerSat_g) {
      pt_controller_B.PProdOut = pt_controller_P.Saturation_LowerSat_g;
    }
  }

  /* End of Saturate: '<S1>/Saturation' */

  /* BusAssignment: '<S2>/Bus Assignment' incorporates:
   *  DataTypeConversion: '<S1>/Data Type Conversion1'
   *  Gain: '<S1>/Gain'
   *  Rounding: '<S1>/Floor'
   */
  rtb_BusAssignment.data = static_cast<real32_T>(floor(static_cast<real_T>
    (pt_controller_P.Gain_Gain_m * static_cast<real32_T>
     (pt_controller_B.PProdOut))));

  /* MATLABSystem: '<S90>/SinkBlock' */
  ros2::matlab::publish_Pub_pt_controller_1693(&rtb_BusAssignment);

  /* Saturate: '<S1>/Positive Torque Demand' */
  if (pt_controller_B.Switch1 > pt_controller_P.PositiveTorqueDemand_UpperSat) {
    pt_controller_B.Switch1 = pt_controller_P.PositiveTorqueDemand_UpperSat;
  } else {
    if (pt_controller_B.Switch1 < pt_controller_P.PositiveTorqueDemand_LowerSat)
    {
      pt_controller_B.Switch1 = pt_controller_P.PositiveTorqueDemand_LowerSat;
    }
  }

  /* End of Saturate: '<S1>/Positive Torque Demand' */

  /* MATLABSystem: '<S9>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_1698
    (&pt_controller_B.b_varargout_2_j);

  /* Outputs for Enabled SubSystem: '<S9>/Enabled Subsystem' */
  pt_controller_EnabledSubsystem(b_varargout_1, &pt_controller_B.b_varargout_2_j,
    &pt_controller_B.EnabledSubsystem_b);

  /* End of Outputs for SubSystem: '<S9>/Enabled Subsystem' */

  /* Product: '<S16>/Engine Torque required' incorporates:
   *  Constant: '<S16>/Constant1'
   *  Constant: '<S16>/Constant2'
   *  Constant: '<S16>/Constant3'
   *  Lookup_n-D: '<S16>/Gear Ratio'
   */
  pt_controller_B.Switch1 = pt_controller_B.Switch1 / pt_controller_P.fd.ratio /
    pt_controller_P.trans.gear_ratio.ratio[plook_u32d_binckan
    (pt_controller_B.EnabledSubsystem_b.In1.data,
     pt_controller_P.trans.gear_ratio.gear, 5U)] /
    pt_controller_P.trans.efficiency / pt_controller_P.fd.efficiency;

  /* MATLABSystem: '<S12>/SourceBlock' */
  b_varargout_1 = ros2::matlab::getLatestMessage_Sub_pt_controller_2591
    (&b_varargout_2);

  /* Outputs for Enabled SubSystem: '<S12>/Enabled Subsystem' */
  pt_controlle_EnabledSubsystem_n(b_varargout_1, &b_varargout_2,
    &pt_controller_B.EnabledSubsystem_i);

  /* End of Outputs for SubSystem: '<S12>/Enabled Subsystem' */

  /* Product: '<S60>/PProd Out' */
  pt_controller_B.Switch1 *= pt_controller_B.EnabledSubsystem_i.In1.data;

  /* Saturate: '<S62>/Saturation' */
  if (pt_controller_B.Switch1 > pt_controller_P.PIDController1_UpperSaturationL)
  {
    /* Saturate: '<S62>/Saturation' */
    pt_controller_B.Switch1 = pt_controller_P.PIDController1_UpperSaturationL;
  } else {
    if (pt_controller_B.Switch1 <
        pt_controller_P.PIDController1_LowerSaturationL) {
      /* Saturate: '<S62>/Saturation' */
      pt_controller_B.Switch1 = pt_controller_P.PIDController1_LowerSaturationL;
    }
  }

  /* End of Saturate: '<S62>/Saturation' */

  /* BusAssignment: '<S2>/Bus Assignment1' incorporates:
   *  DataTypeConversion: '<S2>/Data Type Conversion'
   *  Gain: '<S2>/Gain'
   */
  rtb_BusAssignment1.data = static_cast<real32_T>(pt_controller_P.Gain_Gain_k *
    pt_controller_B.Switch1);

  /* MATLABSystem: '<S91>/SinkBlock' */
  ros2::matlab::publish_Pub_pt_controller_1694(&rtb_BusAssignment1);

  /* Chart: '<S1>/Transmission Shift Logic' incorporates:
   *  DataTypeConversion: '<S1>/Data Type Conversion2'
   *  Lookup_n-D: '<S85>/2-D Lookup Table'
   *  Lookup_n-D: '<S86>/2-D Lookup Table'
   */
  pt_controller_DW.sfEvent = pt_controller_CALL_EVENT;
  if (pt_controller_DW.is_active_c35_pt_controller == 0U) {
    pt_controller_DW.is_active_c35_pt_controller = 1U;
    pt_controller_DW.is_active_gear_state = 1U;
    pt_controller_DW.is_gear_state = pt_controller_IN_first;
    pt_controller_B.Gear = 1.0;
    pt_controller_DW.is_active_selection_state = 1U;
    pt_controller_DW.is_selection_state = pt_controller_IN_steady_state;
  } else {
    if (pt_controller_DW.is_active_gear_state != 0U) {
      pt_controller_gear_state();
    }

    if (pt_controller_DW.is_active_selection_state != 0U) {
      switch (pt_controller_DW.is_selection_state) {
       case pt_controller_IN_downshifting:
        b_previousEvent = pt_controller_DW.sfEvent;
        pt_controller_DW.sfEvent = pt_controller_event_DOWN;
        if (pt_controller_DW.is_active_gear_state != 0U) {
          pt_controller_gear_state();
        }

        pt_controller_DW.sfEvent = b_previousEvent;
        pt_controller_DW.is_selection_state = pt_controller_IN_steady_state;
        break;

       case pt_controller_IN_steady_state:
        /* Outputs for Function Call SubSystem: '<S23>/upshift_map' */
        if (look2_binlxpw(pt_controller_B.Switch1, static_cast<real_T>
                          (pt_controller_B.In1.state.longitudinal_velocity_mps),
                          pt_controller_P.uDLookupTable_bp01Data,
                          pt_controller_P.uDLookupTable_bp02Data,
                          pt_controller_P.uDLookupTable_tableData,
                          pt_controller_P.uDLookupTable_maxIndex, 14U) >
            pt_controller_B.Gear) {
          pt_controller_DW.is_selection_state = pt_controller_IN_upshifting;
        } else {
          /* Outputs for Function Call SubSystem: '<S23>/downshift_map' */
          if ((look2_binlxpw(pt_controller_B.Switch1, static_cast<real_T>
                             (pt_controller_B.In1.state.longitudinal_velocity_mps),
                             pt_controller_P.uDLookupTable_bp01Data_d,
                             pt_controller_P.uDLookupTable_bp02Data_b,
                             pt_controller_P.uDLookupTable_tableData_h,
                             pt_controller_P.uDLookupTable_maxIndex_a, 14U) <
               pt_controller_B.Gear) && (pt_controller_B.Gear > 1.0)) {
            pt_controller_DW.is_selection_state = pt_controller_IN_downshifting;
          }

          /* End of Outputs for SubSystem: '<S23>/downshift_map' */
        }

        /* End of Outputs for SubSystem: '<S23>/upshift_map' */
        break;

       case pt_controller_IN_upshifting:
        b_previousEvent = pt_controller_DW.sfEvent;
        pt_controller_DW.sfEvent = pt_controller_event_UP;
        if (pt_controller_DW.is_active_gear_state != 0U) {
          pt_controller_gear_state();
        }

        pt_controller_DW.sfEvent = b_previousEvent;
        pt_controller_DW.is_selection_state = pt_controller_IN_steady_state;
        break;

       default:
        /* Unreachable state, for coverage only */
        pt_controller_DW.is_selection_state = pt_controlle_IN_NO_ACTIVE_CHILD;
        break;
      }
    }
  }

  /* End of Chart: '<S1>/Transmission Shift Logic' */

  /* DataTypeConversion: '<S1>/Data Type Conversion' */
  pt_controller_B.Switch1 = floor(pt_controller_B.Gear);
  if (rtIsNaN(pt_controller_B.Switch1) || rtIsInf(pt_controller_B.Switch1)) {
    pt_controller_B.Switch1 = 0.0;
  } else {
    pt_controller_B.Switch1 = fmod(pt_controller_B.Switch1, 256.0);
  }

  /* BusAssignment: '<S2>/Bus Assignment2' incorporates:
   *  DataTypeConversion: '<S1>/Data Type Conversion'
   */
  rtb_BusAssignment2.data = static_cast<uint8_T>(pt_controller_B.Switch1 < 0.0 ?
    static_cast<int32_T>(static_cast<uint8_T>(-static_cast<int8_T>
    (static_cast<uint8_T>(-pt_controller_B.Switch1)))) : static_cast<int32_T>(
    static_cast<uint8_T>(pt_controller_B.Switch1)));

  /* MATLABSystem: '<S92>/SinkBlock' */
  ros2::matlab::publish_Pub_pt_controller_1955(&rtb_BusAssignment2);

  /* Switch: '<S80>/Switch' incorporates:
   *  Constant: '<S80>/Constant'
   *  Logic: '<S80>/AND'
   *  Product: '<S80>/Product3'
   *  RelationalOperator: '<S80>/Equal'
   *  RelationalOperator: '<S80>/Equal1'
   */
  if ((pt_controller_B.Switch2_o != pt_controller_B.Add4) &&
      (pt_controller_B.Add4 * pt_controller_B.UkYk1 <
       pt_controller_P.Constant_Value_j5)) {
    /* Update for Delay: '<S80>/Delay' incorporates:
     *  Constant: '<S80>/Constant3'
     */
    pt_controller_DW.Delay_DSTATE = pt_controller_P.Constant3_Value;
  } else {
    /* Update for Delay: '<S80>/Delay' */
    pt_controller_DW.Delay_DSTATE = pt_controller_B.UkYk1;
  }

  /* End of Switch: '<S80>/Switch' */

  /* Update for UnitDelay: '<S80>/Unit Delay1' */
  pt_controller_DW.UnitDelay1_DSTATE = rtb_i;

  /* Update for UnitDelay: '<S80>/Unit Delay2' incorporates:
   *  Constant: '<S80>/Constant1'
   */
  pt_controller_DW.UnitDelay2_DSTATE = pt_controller_P.Constant1_Value_n;

  /* Update for UnitDelay: '<S79>/Unit Delay2' incorporates:
   *  Constant: '<S79>/Constant1'
   */
  pt_controller_DW.UnitDelay2_DSTATE_c = pt_controller_P.Constant1_Value_i;
}

/* Model initialize function */
void pt_controllerModelClass::initialize()
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* non-finite (run-time) assignments */
  pt_controller_P.NegativeForceDemand_LowerSat = rtMinusInf;
  pt_controller_P.PositiveTorqueDemand_UpperSat = rtInf;

  /* Start for MATLABSystem: '<S13>/SourceBlock' */
  pt_controller_DW.obj_m.isInitialized = 0;
  pt_controller_DW.obj_m.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_c = true;
  SystemCore_setup_ctnkblhgrokw4(&pt_controller_DW.obj_m);

  /* Start for MATLABSystem: '<S7>/SourceBlock' */
  pt_controller_DW.obj_c.isInitialized = 0;
  pt_controller_DW.obj_c.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_dd = true;
  pt_con_SystemCore_setup_ctnkblh(&pt_controller_DW.obj_c);

  /* Start for MATLABSystem: '<S6>/SourceBlock' */
  pt_controller_DW.obj_a.isInitialized = 0;
  pt_controller_DW.obj_a.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_p = true;
  pt_cont_SystemCore_setup_ctnkbl(&pt_controller_DW.obj_a);

  /* Start for MATLABSystem: '<S4>/SourceBlock' */
  pt_controller_DW.obj_fm.isInitialized = 0;
  pt_controller_DW.obj_fm.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_m = true;
  pt_contro_SystemCore_setup_ctnk(&pt_controller_DW.obj_fm);

  /* Start for MATLABSystem: '<S5>/SourceBlock' */
  pt_controller_DW.obj_ph.isInitialized = 0;
  pt_controller_DW.obj_ph.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_l = true;
  pt_contr_SystemCore_setup_ctnkb(&pt_controller_DW.obj_ph);

  /* Start for MATLABSystem: '<S8>/SourceBlock' */
  pt_controller_DW.obj_n.isInitialized = 0;
  pt_controller_DW.obj_n.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_cx = true;
  pt_co_SystemCore_setup_ctnkblhg(&pt_controller_DW.obj_n);

  /* Start for MATLABSystem: '<S3>/SourceBlock' */
  pt_controller_DW.obj_af.isInitialized = 0;
  pt_controller_DW.obj_af.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_ll = true;
  pt_control_SystemCore_setup_ctn(&pt_controller_DW.obj_af);

  /* Start for MATLABSystem: '<S10>/SourceBlock' */
  pt_controller_DW.obj_j3.isInitialized = 0;
  pt_controller_DW.obj_j3.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_h = true;
  pt__SystemCore_setup_ctnkblhgro(&pt_controller_DW.obj_j3);

  /* Start for MATLABSystem: '<S11>/SourceBlock' */
  pt_controller_DW.obj_o.isInitialized = 0;
  pt_controller_DW.obj_o.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_c1 = true;
  pt_SystemCore_setup_ctnkblhgrok(&pt_controller_DW.obj_o);

  /* Start for MATLABSystem: '<S14>/SourceBlock' */
  pt_controller_DW.obj_k.isInitialized = 0;
  pt_controller_DW.obj_k.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty = true;
  SystemCore_setup_ctnkblhgrokw4x(&pt_controller_DW.obj_k);

  /* Start for MATLABSystem: '<S90>/SinkBlock' */
  pt_controller_DW.obj_p.isInitialized = 0;
  pt_controller_DW.obj_p.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_o = true;
  pt_controller_SystemCore_setup(&pt_controller_DW.obj_p);

  /* Start for MATLABSystem: '<S9>/SourceBlock' */
  pt_controller_DW.obj_f.isInitialized = 0;
  pt_controller_DW.obj_f.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_d = true;
  pt_c_SystemCore_setup_ctnkblhgr(&pt_controller_DW.obj_f);

  /* Start for MATLABSystem: '<S12>/SourceBlock' */
  pt_controller_DW.obj_h.isInitialized = 0;
  pt_controller_DW.obj_h.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_k = true;
  p_SystemCore_setup_ctnkblhgrokw(&pt_controller_DW.obj_h);

  /* Start for MATLABSystem: '<S91>/SinkBlock' */
  pt_controller_DW.obj_j.isInitialized = 0;
  pt_controller_DW.obj_j.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_f = true;
  pt_controlle_SystemCore_setup_c(&pt_controller_DW.obj_j);

  /* Start for MATLABSystem: '<S92>/SinkBlock' */
  pt_controller_DW.obj.isInitialized = 0;
  pt_controller_DW.obj.matlabCodegenIsDeleted = false;
  pt_controller_DW.objisempty_b = true;
  pt_controll_SystemCore_setup_ct(&pt_controller_DW.obj);

  /* InitializeConditions for Sum: '<S17>/Difference Inputs2' incorporates:
   *  UnitDelay: '<S17>/Delay Input2'
   */
  pt_controller_DW.DelayInput2_DSTATE =
    pt_controller_P.DelayInput2_InitialCondition;

  /* InitializeConditions for Delay: '<S80>/Delay' */
  pt_controller_DW.Delay_DSTATE = pt_controller_P.Delay_InitialCondition;

  /* InitializeConditions for UnitDelay: '<S80>/Unit Delay1' */
  pt_controller_DW.UnitDelay1_DSTATE =
    pt_controller_P.UnitDelay1_InitialCondition;

  /* InitializeConditions for UnitDelay: '<S80>/Unit Delay2' */
  pt_controller_DW.UnitDelay2_DSTATE =
    pt_controller_P.UnitDelay2_InitialCondition;

  /* InitializeConditions for Sum: '<S80>/Add2' incorporates:
   *  UnitDelay: '<S80>/Unit Delay'
   */
  pt_controller_DW.UnitDelay_DSTATE = pt_controller_P.UnitDelay_InitialCondition;

  /* InitializeConditions for Sum: '<S79>/Add3' incorporates:
   *  UnitDelay: '<S79>/Unit Delay1'
   */
  pt_controller_DW.UnitDelay1_DSTATE_d =
    pt_controller_P.UnitDelay1_InitialCondition_j;

  /* InitializeConditions for UnitDelay: '<S79>/Unit Delay2' */
  pt_controller_DW.UnitDelay2_DSTATE_c =
    pt_controller_P.UnitDelay2_InitialCondition_j;

  /* InitializeConditions for Sum: '<S79>/Add2' incorporates:
   *  UnitDelay: '<S79>/Unit Delay'
   */
  pt_controller_DW.UnitDelay_DSTATE_e =
    pt_controller_P.UnitDelay_InitialCondition_k;

  /* SystemInitialize for Enabled SubSystem: '<S13>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_d0,
    &pt_controller_P.EnabledSubsystem_d0);

  /* End of SystemInitialize for SubSystem: '<S13>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S7>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_j,
    &pt_controller_P.EnabledSubsystem_j);

  /* End of SystemInitialize for SubSystem: '<S7>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S6>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_l,
    &pt_controller_P.EnabledSubsystem_l);

  /* End of SystemInitialize for SubSystem: '<S6>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S4>/Enabled Subsystem' */
  pt_contro_EnabledSubsystem_Init(&pt_controller_B.EnabledSubsystem_d,
    &pt_controller_P.EnabledSubsystem_d);

  /* End of SystemInitialize for SubSystem: '<S4>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S5>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_n,
    &pt_controller_P.EnabledSubsystem_n);

  /* End of SystemInitialize for SubSystem: '<S5>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S8>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_ni,
    &pt_controller_P.EnabledSubsystem_ni);

  /* End of SystemInitialize for SubSystem: '<S8>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S3>/Enabled Subsystem' */
  /* SystemInitialize for Outport: '<S93>/Out1' incorporates:
   *  Inport: '<S93>/In1'
   */
  pt_controller_B.In1 = pt_controller_P.Out1_Y0;

  /* End of SystemInitialize for SubSystem: '<S3>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S10>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_dt,
    &pt_controller_P.EnabledSubsystem_dt);

  /* End of SystemInitialize for SubSystem: '<S10>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S11>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_k,
    &pt_controller_P.EnabledSubsystem_k);

  /* End of SystemInitialize for SubSystem: '<S11>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S14>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_g,
    &pt_controller_P.EnabledSubsystem_g);

  /* End of SystemInitialize for SubSystem: '<S14>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S9>/Enabled Subsystem' */
  pt_contro_EnabledSubsystem_Init(&pt_controller_B.EnabledSubsystem_b,
    &pt_controller_P.EnabledSubsystem_b);

  /* End of SystemInitialize for SubSystem: '<S9>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S12>/Enabled Subsystem' */
  pt_cont_EnabledSubsystem_m_Init(&pt_controller_B.EnabledSubsystem_i,
    &pt_controller_P.EnabledSubsystem_i);

  /* End of SystemInitialize for SubSystem: '<S12>/Enabled Subsystem' */

  /* SystemInitialize for Chart: '<S1>/Transmission Shift Logic' */
  pt_controller_DW.sfEvent = pt_controller_CALL_EVENT;
}

/* Model terminate function */
void pt_controllerModelClass::terminate()
{
  /* Terminate for MATLABSystem: '<S13>/SourceBlock' */
  if (!pt_controller_DW.obj_m.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_m.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S13>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S7>/SourceBlock' */
  if (!pt_controller_DW.obj_c.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_c.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S7>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S6>/SourceBlock' */
  if (!pt_controller_DW.obj_a.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_a.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S6>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S4>/SourceBlock' */
  if (!pt_controller_DW.obj_fm.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_fm.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S4>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S5>/SourceBlock' */
  if (!pt_controller_DW.obj_ph.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_ph.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S5>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S8>/SourceBlock' */
  if (!pt_controller_DW.obj_n.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_n.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S8>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S3>/SourceBlock' */
  if (!pt_controller_DW.obj_af.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_af.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S3>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S10>/SourceBlock' */
  if (!pt_controller_DW.obj_j3.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_j3.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S10>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S11>/SourceBlock' */
  if (!pt_controller_DW.obj_o.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_o.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S11>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S14>/SourceBlock' */
  if (!pt_controller_DW.obj_k.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_k.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S14>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S90>/SinkBlock' */
  if (!pt_controller_DW.obj_p.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_p.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S90>/SinkBlock' */

  /* Terminate for MATLABSystem: '<S9>/SourceBlock' */
  if (!pt_controller_DW.obj_f.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_f.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S9>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S12>/SourceBlock' */
  if (!pt_controller_DW.obj_h.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_h.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S12>/SourceBlock' */

  /* Terminate for MATLABSystem: '<S91>/SinkBlock' */
  if (!pt_controller_DW.obj_j.matlabCodegenIsDeleted) {
    pt_controller_DW.obj_j.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S91>/SinkBlock' */

  /* Terminate for MATLABSystem: '<S92>/SinkBlock' */
  if (!pt_controller_DW.obj.matlabCodegenIsDeleted) {
    pt_controller_DW.obj.matlabCodegenIsDeleted = true;
  }

  /* End of Terminate for MATLABSystem: '<S92>/SinkBlock' */
}

/* Constructor */
pt_controllerModelClass::pt_controllerModelClass() :
  pt_controller_B(),
  pt_controller_DW(),
  pt_controller_M()
{
  /* Currently there is no constructor body generated.*/
}

/* Destructor */
pt_controllerModelClass::~pt_controllerModelClass()
{
  /* Currently there is no destructor body generated.*/
}

/* Real-Time Model get method */
RT_MODEL_pt_controller_T * pt_controllerModelClass::getRTM()
{
  return (&pt_controller_M);
}
