// Copyright 2019-2021 The MathWorks, Inc.
// Generated 13-Apr-2021 23:22:18
#ifndef _ROS2_MATLAB_NODEINTERFACE_
#define _ROS2_MATLAB_NODEINTERFACE_
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4265)
#pragma warning(disable : 4458)
#pragma warning(disable : 4100)
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wshadow"
#endif //_MSC_VER
#include "rclcpp/rclcpp.hpp"
class pt_controllerModelClass;
#include "pt_controller_types.h"
#include "slros_busmsg_conversion.h"
#include "std_msgs/msg/float32.hpp"
#include "std_msgs/msg/float64.hpp"
#include "std_msgs/msg/u_int8.hpp"
namespace ros2 {
namespace matlab {
  //Semaphore using std::CV and std::mutex
  class Semaphore {
  public:
    std::mutex mMutex;
    std::condition_variable mCV;
    std::atomic_uint mCount;
    //
    Semaphore(int count = 0)
      : mCount(count) {
      }
    //
    inline void notify() {
      std::unique_lock<std::mutex> lock(mMutex);
      mCount++;
      mCV.notify_all();
    }
    //
    inline void wait() {
      std::unique_lock<std::mutex> lock(mMutex);
      while (mCount == 0) {
        mCV.wait(lock);
      }
      if (mCount)
        mCount--;
    }
  };
  //NodeInterface
  class NodeInterface {
    NodeInterface(const NodeInterface& );
    NodeInterface& operator=(const NodeInterface& );
    //
    rclcpp::Node::SharedPtr mNode;
    std::shared_ptr<pt_controllerModelClass> mModel;
    rclcpp::executors::MultiThreadedExecutor::SharedPtr mExec;
    //
    Semaphore mBaseRateSem;
    std::shared_ptr<std::thread> mBaseRateThread;
    std::shared_ptr<std::thread> mSchedulerThread;
    //
    //
    Semaphore mStopSem;
    volatile boolean_T mRunModel;
    //
    // pt_controller/Raptor/Publish
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr mPub_pt_controller_1693;
    // pt_controller/Raptor/Publish1
    rclcpp::Publisher<std_msgs::msg::Float32>::SharedPtr mPub_pt_controller_1694;
    // pt_controller/Raptor/Publish2
    rclcpp::Publisher<std_msgs::msg::UInt8>::SharedPtr mPub_pt_controller_1955;
    //
    // pt_controller/Subscribe
    rclcpp::Subscription<std_msgs::msg::Float64>::SharedPtr mSub_pt_controller_1695;
    std_msgs::msg::Float64::SharedPtr mLatestMsg_Sub_pt_controller_1695;
    std::mutex mtx_Sub_pt_controller_1695;
    // pt_controller/Subscribe1
    rclcpp::Subscription<std_msgs::msg::Float64>::SharedPtr mSub_pt_controller_1696;
    std_msgs::msg::Float64::SharedPtr mLatestMsg_Sub_pt_controller_1696;
    std::mutex mtx_Sub_pt_controller_1696;
    // pt_controller/Subscribe10
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2594;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2594;
    std::mutex mtx_Sub_pt_controller_2594;
    // pt_controller/Subscribe11
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2595;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2595;
    std::mutex mtx_Sub_pt_controller_2595;
    // pt_controller/Subscribe12
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2596;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2596;
    std::mutex mtx_Sub_pt_controller_2596;
    // pt_controller/Subscribe13
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2597;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2597;
    std::mutex mtx_Sub_pt_controller_2597;
    // pt_controller/Subscribe3
    rclcpp::Subscription<std_msgs::msg::Float64>::SharedPtr mSub_pt_controller_1698;
    std_msgs::msg::Float64::SharedPtr mLatestMsg_Sub_pt_controller_1698;
    std::mutex mtx_Sub_pt_controller_1698;
    // pt_controller/Subscribe5
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2589;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2589;
    std::mutex mtx_Sub_pt_controller_2589;
    // pt_controller/Subscribe6
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2590;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2590;
    std::mutex mtx_Sub_pt_controller_2590;
    // pt_controller/Subscribe7
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2591;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2591;
    std::mutex mtx_Sub_pt_controller_2591;
    // pt_controller/Subscribe8
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2592;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2592;
    std::mutex mtx_Sub_pt_controller_2592;
    // pt_controller/Subscribe9
    rclcpp::Subscription<std_msgs::msg::Float32>::SharedPtr mSub_pt_controller_2593;
    std_msgs::msg::Float32::SharedPtr mLatestMsg_Sub_pt_controller_2593;
    std::mutex mtx_Sub_pt_controller_2593;
  public:
    NodeInterface();
    ~NodeInterface();
    //
    void initialize(int argc, char * const argv[]);
    int run();
    void stop(void);
    void terminate(void);
    //
    boolean_T getStopRequestedFlag(void);
    void schedulerThreadCallback(void);
    void baseRateTask(void);
    // pt_controller/Raptor/Publish
    void create_Pub_pt_controller_1693(const char *topicName, const rmw_qos_profile_t& qosProfile);
    void publish_Pub_pt_controller_1693(const SL_Bus_std_msgs_Float32* inBus);
    // pt_controller/Raptor/Publish1
    void create_Pub_pt_controller_1694(const char *topicName, const rmw_qos_profile_t& qosProfile);
    void publish_Pub_pt_controller_1694(const SL_Bus_std_msgs_Float32* inBus);
    // pt_controller/Raptor/Publish2
    void create_Pub_pt_controller_1955(const char *topicName, const rmw_qos_profile_t& qosProfile);
    void publish_Pub_pt_controller_1955(const SL_Bus_std_msgs_UInt8* inBus);
    // pt_controller/Subscribe
    void create_Sub_pt_controller_1695(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_1695(SL_Bus_std_msgs_Float64* outBus);
    // pt_controller/Subscribe1
    void create_Sub_pt_controller_1696(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_1696(SL_Bus_std_msgs_Float64* outBus);
    // pt_controller/Subscribe10
    void create_Sub_pt_controller_2594(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2594(SL_Bus_std_msgs_Float32* outBus);
    // pt_controller/Subscribe11
    void create_Sub_pt_controller_2595(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2595(SL_Bus_std_msgs_Float32* outBus);
    // pt_controller/Subscribe12
    void create_Sub_pt_controller_2596(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2596(SL_Bus_std_msgs_Float32* outBus);
    // pt_controller/Subscribe13
    void create_Sub_pt_controller_2597(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2597(SL_Bus_std_msgs_Float32* outBus);
    // pt_controller/Subscribe3
    void create_Sub_pt_controller_1698(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_1698(SL_Bus_std_msgs_Float64* outBus);
    // pt_controller/Subscribe5
    void create_Sub_pt_controller_2589(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2589(SL_Bus_std_msgs_Float32* outBus);
    // pt_controller/Subscribe6
    void create_Sub_pt_controller_2590(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2590(SL_Bus_std_msgs_Float32* outBus);
    // pt_controller/Subscribe7
    void create_Sub_pt_controller_2591(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2591(SL_Bus_std_msgs_Float32* outBus);
    // pt_controller/Subscribe8
    void create_Sub_pt_controller_2592(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2592(SL_Bus_std_msgs_Float32* outBus);
    // pt_controller/Subscribe9
    void create_Sub_pt_controller_2593(const char *topicName, const rmw_qos_profile_t& qosProfile);
    bool getLatestMessage_Sub_pt_controller_2593(SL_Bus_std_msgs_Float32* outBus);
    //
    rclcpp::Node::SharedPtr getNode() {
      return mNode;
    }
    //
    std::shared_ptr<pt_controllerModelClass> getModel() {
      return mModel;
    }
  }; //class NodeInterface
  //
  std::shared_ptr<ros2::matlab::NodeInterface> getNodeInterface();
  // Helper for pt_controller/Raptor/Publish
  void create_Pub_pt_controller_1693(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  void publish_Pub_pt_controller_1693(const SL_Bus_std_msgs_Float32* inBus);
  // Helper for pt_controller/Raptor/Publish1
  void create_Pub_pt_controller_1694(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  void publish_Pub_pt_controller_1694(const SL_Bus_std_msgs_Float32* inBus);
  // Helper for pt_controller/Raptor/Publish2
  void create_Pub_pt_controller_1955(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  void publish_Pub_pt_controller_1955(const SL_Bus_std_msgs_UInt8* inBus);
  // Helper for pt_controller/Subscribe
  void create_Sub_pt_controller_1695(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_1695(SL_Bus_std_msgs_Float64* outBus);
  // Helper for pt_controller/Subscribe1
  void create_Sub_pt_controller_1696(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_1696(SL_Bus_std_msgs_Float64* outBus);
  // Helper for pt_controller/Subscribe10
  void create_Sub_pt_controller_2594(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2594(SL_Bus_std_msgs_Float32* outBus);
  // Helper for pt_controller/Subscribe11
  void create_Sub_pt_controller_2595(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2595(SL_Bus_std_msgs_Float32* outBus);
  // Helper for pt_controller/Subscribe12
  void create_Sub_pt_controller_2596(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2596(SL_Bus_std_msgs_Float32* outBus);
  // Helper for pt_controller/Subscribe13
  void create_Sub_pt_controller_2597(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2597(SL_Bus_std_msgs_Float32* outBus);
  // Helper for pt_controller/Subscribe3
  void create_Sub_pt_controller_1698(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_1698(SL_Bus_std_msgs_Float64* outBus);
  // Helper for pt_controller/Subscribe5
  void create_Sub_pt_controller_2589(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2589(SL_Bus_std_msgs_Float32* outBus);
  // Helper for pt_controller/Subscribe6
  void create_Sub_pt_controller_2590(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2590(SL_Bus_std_msgs_Float32* outBus);
  // Helper for pt_controller/Subscribe7
  void create_Sub_pt_controller_2591(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2591(SL_Bus_std_msgs_Float32* outBus);
  // Helper for pt_controller/Subscribe8
  void create_Sub_pt_controller_2592(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2592(SL_Bus_std_msgs_Float32* outBus);
  // Helper for pt_controller/Subscribe9
  void create_Sub_pt_controller_2593(const char *topicName, const rmw_qos_profile_t& qosProfile = rmw_qos_profile_default);
  bool getLatestMessage_Sub_pt_controller_2593(SL_Bus_std_msgs_Float32* outBus);
  // Get QoS Settings from RMW
  inline rclcpp::QoS getQOSSettingsFromRMW(const rmw_qos_profile_t& qosProfile) {
      rclcpp::QoS qos(rclcpp::QoSInitialization::from_rmw(qosProfile));
      if (RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL == qosProfile.durability) {
          qos.transient_local();
      } else {
          qos.durability_volatile();
      }
      if (RMW_QOS_POLICY_RELIABILITY_RELIABLE == qosProfile.reliability) {
          qos.reliable();
      } else {
          qos.best_effort();
      }
      return qos;
  }
}//namespace matlab
}//namespace ros2
#ifdef _MSC_VER
#pragma warning(pop)
#else
#pragma GCC diagnostic pop
#endif //_MSC_VER
#endif //_ROS2_MATLAB_ROS2CGEN_MULTIRATE_
