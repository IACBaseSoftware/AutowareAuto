/*
 * pt_controller.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "pt_controller".
 *
 * Model version              : 2.39
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Tue Apr 13 23:22:14 2021
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_pt_controller_h_
#define RTW_HEADER_pt_controller_h_
#include <math.h>
#include <stddef.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "pt_controller_pubsub_common.h"
#include "pt_controller_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "MW_target_hardware_resources.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

/* Block signals for system '<S3>/Enabled Subsystem' */
typedef struct {
  SL_Bus_std_msgs_Float64 In1;         /* '<S93>/In1' */
} B_EnabledSubsystem_pt_control_T;

/* Block signals for system '<S5>/Enabled Subsystem' */
typedef struct {
  SL_Bus_std_msgs_Float32 In1;         /* '<S95>/In1' */
} B_EnabledSubsystem_pt_contr_g_T;

/* Block signals (default storage) */
typedef struct {
  char_T b_zeroDelimTopic[33];
  char_T b_zeroDelimTopic_m[33];
  char_T b_zeroDelimTopic_c[22];
  char_T b_zeroDelimTopic_k[21];
  char_T b_zeroDelimTopic_cx[21];
  char_T b_zeroDelimTopic_b[20];
  char_T b_zeroDelimTopic_p[16];
  char_T b_zeroDelimTopic_cv[16];
  char_T b_zeroDelimTopic_f[15];
  char_T b_zeroDelimTopic_g[15];
  char_T b_zeroDelimTopic_g1[15];
  char_T b_zeroDelimTopic_me[14];
  char_T b_zeroDelimTopic_n[14];
  char_T b_zeroDelimTopic_pp[14];
  char_T b_zeroDelimTopic_l[13];
  real_T Gear;                         /* '<S1>/Transmission Shift Logic' */
  real_T Add4;                         /* '<S80>/Add4' */
  real_T PProdOut;                     /* '<S60>/PProd Out' */
  real_T Switch2_o;                    /* '<S84>/Switch2' */
  real_T UkYk1;                        /* '<S17>/Difference Inputs1' */
  real_T Switch1;                      /* '<S81>/Switch1' */
  SL_Bus_std_msgs_Float64 b_varargout_2;
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_g;/* '<S14>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_d0;/* '<S13>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_i;/* '<S12>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_k;/* '<S11>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_dt;/* '<S10>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_control_T EnabledSubsystem_b;/* '<S9>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_ni;/* '<S8>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_j;/* '<S7>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_l;/* '<S6>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_contr_g_T EnabledSubsystem_n;/* '<S5>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_control_T EnabledSubsystem_d;/* '<S4>/Enabled Subsystem' */
  B_EnabledSubsystem_pt_control_T EnabledSubsystem;/* '<S3>/Enabled Subsystem' */
} B_pt_controller_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  ros_slros2_internal_block_Pub_T obj; /* '<S92>/SinkBlock' */
  ros_slros2_internal_block_Pub_T obj_j;/* '<S91>/SinkBlock' */
  ros_slros2_internal_block_Pub_T obj_p;/* '<S90>/SinkBlock' */
  ros_slros2_internal_block_Sub_T obj_k;/* '<S14>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_m;/* '<S13>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_h;/* '<S12>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_o;/* '<S11>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_j3;/* '<S10>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_f;/* '<S9>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_n;/* '<S8>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_c;/* '<S7>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_a;/* '<S6>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_ph;/* '<S5>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_fm;/* '<S4>/SourceBlock' */
  ros_slros2_internal_block_Sub_T obj_af;/* '<S3>/SourceBlock' */
  real_T DelayInput2_DSTATE;           /* '<S17>/Delay Input2' */
  real_T Delay_DSTATE;                 /* '<S80>/Delay' */
  real_T UnitDelay1_DSTATE;            /* '<S80>/Unit Delay1' */
  real_T UnitDelay_DSTATE;             /* '<S80>/Unit Delay' */
  real_T UnitDelay1_DSTATE_d;          /* '<S79>/Unit Delay1' */
  real_T UnitDelay_DSTATE_e;           /* '<S79>/Unit Delay' */
  int32_T sfEvent;                     /* '<S1>/Transmission Shift Logic' */
  boolean_T UnitDelay2_DSTATE;         /* '<S80>/Unit Delay2' */
  boolean_T UnitDelay2_DSTATE_c;       /* '<S79>/Unit Delay2' */
  uint8_T is_active_c35_pt_controller; /* '<S1>/Transmission Shift Logic' */
  uint8_T is_gear_state;               /* '<S1>/Transmission Shift Logic' */
  uint8_T is_active_gear_state;        /* '<S1>/Transmission Shift Logic' */
  uint8_T is_selection_state;          /* '<S1>/Transmission Shift Logic' */
  uint8_T is_active_selection_state;   /* '<S1>/Transmission Shift Logic' */
  boolean_T objisempty;                /* '<S14>/SourceBlock' */
  boolean_T objisempty_c;              /* '<S13>/SourceBlock' */
  boolean_T objisempty_k;              /* '<S12>/SourceBlock' */
  boolean_T objisempty_c1;             /* '<S11>/SourceBlock' */
  boolean_T objisempty_h;              /* '<S10>/SourceBlock' */
  boolean_T objisempty_d;              /* '<S9>/SourceBlock' */
  boolean_T objisempty_cx;             /* '<S8>/SourceBlock' */
  boolean_T objisempty_dd;             /* '<S7>/SourceBlock' */
  boolean_T objisempty_p;              /* '<S6>/SourceBlock' */
  boolean_T objisempty_l;              /* '<S5>/SourceBlock' */
  boolean_T objisempty_m;              /* '<S4>/SourceBlock' */
  boolean_T objisempty_ll;             /* '<S3>/SourceBlock' */
  boolean_T objisempty_b;              /* '<S92>/SinkBlock' */
  boolean_T objisempty_f;              /* '<S91>/SinkBlock' */
  boolean_T objisempty_o;              /* '<S90>/SinkBlock' */
} DW_pt_controller_T;

/* Parameters for system: '<S3>/Enabled Subsystem' */
struct P_EnabledSubsystem_pt_control_T_ {
  SL_Bus_std_msgs_Float64 Out1_Y0;     /* Computed Parameter: Out1_Y0
                                        * Referenced by: '<S93>/Out1'
                                        */
};

/* Parameters for system: '<S5>/Enabled Subsystem' */
struct P_EnabledSubsystem_pt_contr_l_T_ {
  SL_Bus_std_msgs_Float32 Out1_Y0;     /* Computed Parameter: Out1_Y0
                                        * Referenced by: '<S95>/Out1'
                                        */
};

/* Parameters (default storage) */
struct P_pt_controller_T_ {
  struct_5ByvgBbasQbyA349mhE0EE trans; /* Variable: trans
                                        * Referenced by:
                                        *   '<S16>/Constant2'
                                        *   '<S16>/Gear Ratio'
                                        */
  struct_GxrHM0sYOdie5URlXo3lH fd;     /* Variable: fd
                                        * Referenced by:
                                        *   '<S16>/Constant1'
                                        *   '<S16>/Constant3'
                                        */
  real_T Bore_BCF;                     /* Variable: Bore_BCF
                                        * Referenced by: '<S74>/Caliper pad bore'
                                        */
  real_T Cd;                           /* Variable: Cd
                                        * Referenced by: '<S78>/Constant9'
                                        */
  real_T Cl;                           /* Variable: Cl
                                        * Referenced by: '<S75>/Constant15'
                                        */
  real_T Re_fl;                        /* Variable: Re_fl
                                        * Referenced by: '<S73>/Gain'
                                        */
  real_T Re_rr;                        /* Variable: Re_rr
                                        * Referenced by: '<S20>/Constant'
                                        */
  real_T Rm_F;                         /* Variable: Rm_F
                                        * Referenced by: '<S73>/Gain2'
                                        */
  real_T bias;                         /* Variable: bias
                                        * Referenced by: '<S1>/Constant'
                                        */
  real_T dt_controller;                /* Variable: dt_controller
                                        * Referenced by: '<S80>/dT'
                                        */
  real_T m;                            /* Variable: m
                                        * Referenced by: '<S76>/Constant7'
                                        */
  real_T rho;                          /* Variable: rho
                                        * Referenced by: '<S78>/Constant10'
                                        */
  real_T PIDController1_LowerSaturationL;
                              /* Mask Parameter: PIDController1_LowerSaturationL
                               * Referenced by: '<S62>/Saturation'
                               */
  real_T PIDController1_UpperSaturationL;
                              /* Mask Parameter: PIDController1_UpperSaturationL
                               * Referenced by: '<S62>/Saturation'
                               */
  SL_Bus_std_msgs_Float32 Constant_Value;/* Computed Parameter: Constant_Value
                                          * Referenced by: '<S87>/Constant'
                                          */
  SL_Bus_std_msgs_Float32 Constant_Value_k;/* Computed Parameter: Constant_Value_k
                                            * Referenced by: '<S13>/Constant'
                                            */
  SL_Bus_std_msgs_Float32 Constant_Value_j;/* Computed Parameter: Constant_Value_j
                                            * Referenced by: '<S7>/Constant'
                                            */
  SL_Bus_std_msgs_Float32 Constant_Value_f;/* Computed Parameter: Constant_Value_f
                                            * Referenced by: '<S6>/Constant'
                                            */
  SL_Bus_std_msgs_Float32 Constant_Value_l;/* Computed Parameter: Constant_Value_l
                                            * Referenced by: '<S5>/Constant'
                                            */
  SL_Bus_std_msgs_Float32 Constant_Value_p;/* Computed Parameter: Constant_Value_p
                                            * Referenced by: '<S8>/Constant'
                                            */
  SL_Bus_std_msgs_Float32 Constant_Value_jx;/* Computed Parameter: Constant_Value_jx
                                             * Referenced by: '<S10>/Constant'
                                             */
  SL_Bus_std_msgs_Float32 Constant_Value_jk;/* Computed Parameter: Constant_Value_jk
                                             * Referenced by: '<S11>/Constant'
                                             */
  SL_Bus_std_msgs_Float32 Constant_Value_g;/* Computed Parameter: Constant_Value_g
                                            * Referenced by: '<S14>/Constant'
                                            */
  SL_Bus_std_msgs_Float32 Constant_Value_k4;/* Computed Parameter: Constant_Value_k4
                                             * Referenced by: '<S12>/Constant'
                                             */
  SL_Bus_std_msgs_Float32 Constant_Value_je;/* Computed Parameter: Constant_Value_je
                                             * Referenced by: '<S88>/Constant'
                                             */
  SL_Bus_std_msgs_Float64 Constant_Value_h;/* Computed Parameter: Constant_Value_h
                                            * Referenced by: '<S4>/Constant'
                                            */
  SL_Bus_std_msgs_Float64 Constant_Value_km;/* Computed Parameter: Constant_Value_km
                                             * Referenced by: '<S3>/Constant'
                                             */
  SL_Bus_std_msgs_Float64 Constant_Value_gb;/* Computed Parameter: Constant_Value_gb
                                             * Referenced by: '<S9>/Constant'
                                             */
  SL_Bus_std_msgs_UInt8 Constant_Value_a;/* Computed Parameter: Constant_Value_a
                                          * Referenced by: '<S89>/Constant'
                                          */
  real_T Gain_Gain;                    /* Expression: -1
                                        * Referenced by: '<S81>/Gain'
                                        */
  real_T IGain3_Value;                 /* Expression: 0.86
                                        * Referenced by: '<S81>/I Gain3'
                                        */
  real_T IGain2_Value;                 /* Expression: 1
                                        * Referenced by: '<S81>/I Gain2'
                                        */
  real_T Switch2_Threshold;            /* Expression: 50
                                        * Referenced by: '<S81>/Switch2'
                                        */
  real_T IGain1_Value;                 /* Expression: 2.5
                                        * Referenced by: '<S81>/I Gain1'
                                        */
  real_T IGain3_Value_d;               /* Expression: 0.33
                                        * Referenced by: '<S82>/I Gain3'
                                        */
  real_T IGain1_Value_d;               /* Expression: 0.3
                                        * Referenced by: '<S82>/I Gain1'
                                        */
  real_T Switch1_Threshold;            /* Expression: 50
                                        * Referenced by: '<S82>/Switch1'
                                        */
  real_T IGain2_Value_g;               /* Expression: 0.25
                                        * Referenced by: '<S82>/I Gain2'
                                        */
  real_T dGain2_Value;                 /* Expression: 0
                                        * Referenced by: '<S80>/d Gain2'
                                        */
  real_T Constant2_Value;              /* Expression: double(0)
                                        * Referenced by: '<S80>/Constant2'
                                        */
  real_T Constant3_Value;              /* Expression: 0
                                        * Referenced by: '<S80>/Constant3'
                                        */
  real_T dGain2_Value_b;               /* Expression: 0
                                        * Referenced by: '<S79>/d Gain2'
                                        */
  real_T Constant2_Value_m;            /* Expression: double(0)
                                        * Referenced by: '<S79>/Constant2'
                                        */
  real_T PGain_Value;                  /* Expression: 0.6
                                        * Referenced by: '<S79>/P Gain'
                                        */
  real_T Saturation1_UpperSat;         /* Expression: 7
                                        * Referenced by: '<S79>/Saturation1'
                                        */
  real_T Saturation1_LowerSat;         /* Expression: -20
                                        * Referenced by: '<S79>/Saturation1'
                                        */
  real_T upshift_Y0;                   /* Computed Parameter: upshift_Y0
                                        * Referenced by: '<S86>/upshift'
                                        */
  real_T uDLookupTable_tableData[2506];
  /* Expression: [1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6]

   * Referenced by: '<S86>/2-D Lookup Table'
   */
  real_T uDLookupTable_bp01Data[14];   /* Expression: [1:14]
                                        * Referenced by: '<S86>/2-D Lookup Table'
                                        */
  real_T uDLookupTable_bp02Data[179];  /* Expression: [1:179]
                                        * Referenced by: '<S86>/2-D Lookup Table'
                                        */
  real_T downshift_Y0;                 /* Computed Parameter: downshift_Y0
                                        * Referenced by: '<S85>/downshift'
                                        */
  real_T uDLookupTable_tableData_h[2506];
  /* Expression: [1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6;
     1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	2	2	2	2	2	2	2	2	2	3	3	3	3	3	3	3	3	3	3	3	4	4	4	4	4	4	4	5	5	5	5	5	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6	6]

   * Referenced by: '<S85>/2-D Lookup Table'
   */
  real_T uDLookupTable_bp01Data_d[14]; /* Expression: [1:14]
                                        * Referenced by: '<S85>/2-D Lookup Table'
                                        */
  real_T uDLookupTable_bp02Data_b[179];/* Expression: [1:179]
                                        * Referenced by: '<S85>/2-D Lookup Table'
                                        */
  real_T DelayInput2_InitialCondition; /* Expression: 0
                                        * Referenced by: '<S17>/Delay Input2'
                                        */
  real_T sampletime_WtEt;              /* Computed Parameter: sampletime_WtEt
                                        * Referenced by: '<S17>/sample time'
                                        */
  real_T Delay_InitialCondition;       /* Expression: 0.0
                                        * Referenced by: '<S80>/Delay'
                                        */
  real_T UnitDelay1_InitialCondition;  /* Expression: 0
                                        * Referenced by: '<S80>/Unit Delay1'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 2
                                        * Referenced by: '<S80>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: -2
                                        * Referenced by: '<S80>/Saturation'
                                        */
  real_T UnitDelay_InitialCondition;   /* Expression: 0
                                        * Referenced by: '<S80>/Unit Delay'
                                        */
  real_T Switch1_Threshold_j;          /* Expression: 25
                                        * Referenced by: '<S81>/Switch1'
                                        */
  real_T Switch_Threshold;             /* Expression: 25
                                        * Referenced by: '<S82>/Switch'
                                        */
  real_T dT_Value;                     /* Expression: 0.04
                                        * Referenced by: '<S79>/dT'
                                        */
  real_T UnitDelay1_InitialCondition_j;/* Expression: 0
                                        * Referenced by: '<S79>/Unit Delay1'
                                        */
  real_T UnitDelay_InitialCondition_k; /* Expression: 0
                                        * Referenced by: '<S79>/Unit Delay'
                                        */
  real_T Constant8_Value;              /* Expression: 0.5
                                        * Referenced by: '<S78>/Constant8'
                                        */
  real_T Constant1_Value;              /* Expression: 1.26
                                        * Referenced by: '<S78>/Constant1'
                                        */
  real_T Cr_Value;                     /* Expression: 0.046
                                        * Referenced by: '<S77>/Cr'
                                        */
  real_T Switch_Threshold_p;           /* Expression: 0
                                        * Referenced by: '<S1>/Switch'
                                        */
  real_T NegativeForceDemand_UpperSat; /* Expression: 0
                                        * Referenced by: '<S1>/Negative Force Demand'
                                        */
  real_T NegativeForceDemand_LowerSat; /* Expression: -inf
                                        * Referenced by: '<S1>/Negative Force Demand'
                                        */
  real_T Gain1_Gain;                   /* Expression: -1
                                        * Referenced by: '<S21>/Gain1'
                                        */
  real_T Gain_Gain_j;                  /* Expression: 0.5
                                        * Referenced by: '<S19>/Gain'
                                        */
  real_T Constant_Value_hd;            /* Expression: 0.45
                                        * Referenced by: '<S73>/Constant'
                                        */
  real_T Gain_Gain_a;                  /* Expression: 1/2
                                        * Referenced by: '<S74>/Gain'
                                        */
  real_T Constant_Value_c;             /* Expression: pi
                                        * Referenced by: '<S74>/Constant'
                                        */
  real_T Saturation_UpperSat_l;        /* Expression: 800*6895
                                        * Referenced by: '<S1>/Saturation'
                                        */
  real_T Saturation_LowerSat_g;        /* Expression: 0
                                        * Referenced by: '<S1>/Saturation'
                                        */
  real_T PositiveTorqueDemand_UpperSat;/* Expression: inf
                                        * Referenced by: '<S1>/Positive Torque Demand'
                                        */
  real_T PositiveTorqueDemand_LowerSat;/* Expression: 0
                                        * Referenced by: '<S1>/Positive Torque Demand'
                                        */
  real_T Gain_Gain_k;                  /* Expression: 100/90
                                        * Referenced by: '<S2>/Gain'
                                        */
  real_T Constant_Value_j5;            /* Expression: 0
                                        * Referenced by: '<S80>/Constant'
                                        */
  real32_T Gain_Gain_m;                /* Computed Parameter: Gain_Gain_m
                                        * Referenced by: '<S1>/Gain'
                                        */
  uint32_T uDLookupTable_maxIndex[2];
                                   /* Computed Parameter: uDLookupTable_maxIndex
                                    * Referenced by: '<S86>/2-D Lookup Table'
                                    */
  uint32_T uDLookupTable_maxIndex_a[2];
                                 /* Computed Parameter: uDLookupTable_maxIndex_a
                                  * Referenced by: '<S85>/2-D Lookup Table'
                                  */
  boolean_T UnitDelay2_InitialCondition;
                              /* Computed Parameter: UnitDelay2_InitialCondition
                               * Referenced by: '<S80>/Unit Delay2'
                               */
  boolean_T UnitDelay2_InitialCondition_j;
                            /* Computed Parameter: UnitDelay2_InitialCondition_j
                             * Referenced by: '<S79>/Unit Delay2'
                             */
  boolean_T Constant1_Value_i;         /* Expression: boolean(0)
                                        * Referenced by: '<S79>/Constant1'
                                        */
  boolean_T Constant1_Value_n;         /* Expression: boolean(0)
                                        * Referenced by: '<S80>/Constant1'
                                        */
  uint8_T ManualSwitch_CurrentSetting;
                              /* Computed Parameter: ManualSwitch_CurrentSetting
                               * Referenced by: '<S22>/Manual Switch'
                               */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_g;/* '<S14>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_d0;/* '<S13>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_i;/* '<S12>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_k;/* '<S11>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_dt;/* '<S10>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_control_T EnabledSubsystem_b;/* '<S9>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_ni;/* '<S8>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_j;/* '<S7>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_l;/* '<S6>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_contr_l_T EnabledSubsystem_n;/* '<S5>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_control_T EnabledSubsystem_d;/* '<S4>/Enabled Subsystem' */
  P_EnabledSubsystem_pt_control_T EnabledSubsystem;/* '<S3>/Enabled Subsystem' */
};

/* Real-time Model Data Structure */
struct tag_RTM_pt_controller_T {
  const char_T *errorStatus;
};

/* Class declaration for model pt_controller */
class pt_controllerModelClass {
  /* public data and function members */
 public:
  /* model initialize function */
  void initialize();

  /* model step function */
  void step();

  /* model terminate function */
  void terminate();

  /* Constructor */
  pt_controllerModelClass();

  /* Destructor */
  ~pt_controllerModelClass();

  /* Real-Time Model get method */
  RT_MODEL_pt_controller_T * getRTM();

  /* private data and function members */
 private:
  /* Tunable parameters */
  static P_pt_controller_T pt_controller_P;

  /* Block signals */
  B_pt_controller_T pt_controller_B;

  /* Block states */
  DW_pt_controller_T pt_controller_DW;

  /* Real-Time Model */
  RT_MODEL_pt_controller_T pt_controller_M;

  /* private member function(s) for subsystem '<S3>/Enabled Subsystem'*/
  static void pt_contro_EnabledSubsystem_Init(B_EnabledSubsystem_pt_control_T
    *localB, P_EnabledSubsystem_pt_control_T *localP);
  static void pt_controller_EnabledSubsystem(boolean_T rtu_Enable, const
    SL_Bus_std_msgs_Float64 *rtu_In1, B_EnabledSubsystem_pt_control_T *localB);

  /* private member function(s) for subsystem '<S5>/Enabled Subsystem'*/
  static void pt_cont_EnabledSubsystem_m_Init(B_EnabledSubsystem_pt_contr_g_T
    *localB, P_EnabledSubsystem_pt_contr_l_T *localP);
  static void pt_controlle_EnabledSubsystem_n(boolean_T rtu_Enable, const
    SL_Bus_std_msgs_Float32 *rtu_In1, B_EnabledSubsystem_pt_contr_g_T *localB);

  /* private member function(s) for subsystem '<Root>'*/
  void SystemCore_setup_ctnkblhgrokw4(ros_slros2_internal_block_Sub_T *obj);
  void pt_con_SystemCore_setup_ctnkblh(ros_slros2_internal_block_Sub_T *obj);
  void pt_cont_SystemCore_setup_ctnkbl(ros_slros2_internal_block_Sub_T *obj);
  void pt_contro_SystemCore_setup_ctnk(ros_slros2_internal_block_Sub_T *obj);
  void pt_contr_SystemCore_setup_ctnkb(ros_slros2_internal_block_Sub_T *obj);
  void pt_co_SystemCore_setup_ctnkblhg(ros_slros2_internal_block_Sub_T *obj);
  void pt_control_SystemCore_setup_ctn(ros_slros2_internal_block_Sub_T *obj);
  void pt__SystemCore_setup_ctnkblhgro(ros_slros2_internal_block_Sub_T *obj);
  void pt_SystemCore_setup_ctnkblhgrok(ros_slros2_internal_block_Sub_T *obj);
  void SystemCore_setup_ctnkblhgrokw4x(ros_slros2_internal_block_Sub_T *obj);
  void pt_controller_SystemCore_setup(ros_slros2_internal_block_Pub_T *obj);
  void pt_c_SystemCore_setup_ctnkblhgr(ros_slros2_internal_block_Sub_T *obj);
  void p_SystemCore_setup_ctnkblhgrokw(ros_slros2_internal_block_Sub_T *obj);
  void pt_controlle_SystemCore_setup_c(ros_slros2_internal_block_Pub_T *obj);
  void pt_controll_SystemCore_setup_ct(ros_slros2_internal_block_Pub_T *obj);
  void pt_controller_gear_state(void);
};

extern volatile boolean_T stopRequested;
extern volatile boolean_T runModel;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S17>/FixPt Data Type Duplicate' : Unused code path elimination
 * Block '<S72>/Data Type Duplicate' : Unused code path elimination
 * Block '<S72>/Data Type Propagation' : Unused code path elimination
 * Block '<S18>/Data Type Duplicate' : Unused code path elimination
 * Block '<S18>/Data Type Propagation' : Unused code path elimination
 * Block '<S19>/Gain1' : Unused code path elimination
 * Block '<S73>/Divide1' : Unused code path elimination
 * Block '<S73>/Gain1' : Unused code path elimination
 * Block '<S73>/Gain3' : Unused code path elimination
 * Block '<S74>/Caliper pad bore2' : Unused code path elimination
 * Block '<S74>/Constant2' : Unused code path elimination
 * Block '<S74>/Divide2' : Unused code path elimination
 * Block '<S74>/Gain2' : Unused code path elimination
 * Block '<S74>/Product2' : Unused code path elimination
 * Block '<S74>/Square2' : Unused code path elimination
 * Block '<S21>/Constant' : Unused code path elimination
 * Block '<S21>/Gain2' : Unused code path elimination
 * Block '<S21>/Product1' : Unused code path elimination
 * Block '<S21>/Subtract' : Unused code path elimination
 * Block '<S83>/Data Type Duplicate' : Unused code path elimination
 * Block '<S83>/Data Type Propagation' : Unused code path elimination
 * Block '<S84>/Data Type Duplicate' : Unused code path elimination
 * Block '<S84>/Data Type Propagation' : Unused code path elimination
 * Block '<S2>/Display1' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'pt_controller'
 * '<S1>'   : 'pt_controller/Computer'
 * '<S2>'   : 'pt_controller/Raptor'
 * '<S3>'   : 'pt_controller/Subscribe'
 * '<S4>'   : 'pt_controller/Subscribe1'
 * '<S5>'   : 'pt_controller/Subscribe10'
 * '<S6>'   : 'pt_controller/Subscribe11'
 * '<S7>'   : 'pt_controller/Subscribe12'
 * '<S8>'   : 'pt_controller/Subscribe13'
 * '<S9>'   : 'pt_controller/Subscribe3'
 * '<S10>'  : 'pt_controller/Subscribe5'
 * '<S11>'  : 'pt_controller/Subscribe6'
 * '<S12>'  : 'pt_controller/Subscribe7'
 * '<S13>'  : 'pt_controller/Subscribe8'
 * '<S14>'  : 'pt_controller/Subscribe9'
 * '<S15>'  : 'pt_controller/Computer/PID Controller1'
 * '<S16>'  : 'pt_controller/Computer/Positive wheel torque demand to engine torque demand'
 * '<S17>'  : 'pt_controller/Computer/Rate Limiter Dynamic'
 * '<S18>'  : 'pt_controller/Computer/Saturation Dynamic'
 * '<S19>'  : 'pt_controller/Computer/Subsystem1'
 * '<S20>'  : 'pt_controller/Computer/Subsystem2'
 * '<S21>'  : 'pt_controller/Computer/Subsystem3'
 * '<S22>'  : 'pt_controller/Computer/Subsystem5'
 * '<S23>'  : 'pt_controller/Computer/Transmission Shift Logic'
 * '<S24>'  : 'pt_controller/Computer/PID Controller1/Anti-windup'
 * '<S25>'  : 'pt_controller/Computer/PID Controller1/D Gain'
 * '<S26>'  : 'pt_controller/Computer/PID Controller1/Filter'
 * '<S27>'  : 'pt_controller/Computer/PID Controller1/Filter ICs'
 * '<S28>'  : 'pt_controller/Computer/PID Controller1/I Gain'
 * '<S29>'  : 'pt_controller/Computer/PID Controller1/Ideal P Gain'
 * '<S30>'  : 'pt_controller/Computer/PID Controller1/Ideal P Gain Fdbk'
 * '<S31>'  : 'pt_controller/Computer/PID Controller1/Integrator'
 * '<S32>'  : 'pt_controller/Computer/PID Controller1/Integrator ICs'
 * '<S33>'  : 'pt_controller/Computer/PID Controller1/N Copy'
 * '<S34>'  : 'pt_controller/Computer/PID Controller1/N Gain'
 * '<S35>'  : 'pt_controller/Computer/PID Controller1/P Copy'
 * '<S36>'  : 'pt_controller/Computer/PID Controller1/Parallel P Gain'
 * '<S37>'  : 'pt_controller/Computer/PID Controller1/Reset Signal'
 * '<S38>'  : 'pt_controller/Computer/PID Controller1/Saturation'
 * '<S39>'  : 'pt_controller/Computer/PID Controller1/Saturation Fdbk'
 * '<S40>'  : 'pt_controller/Computer/PID Controller1/Sum'
 * '<S41>'  : 'pt_controller/Computer/PID Controller1/Sum Fdbk'
 * '<S42>'  : 'pt_controller/Computer/PID Controller1/Tracking Mode'
 * '<S43>'  : 'pt_controller/Computer/PID Controller1/Tracking Mode Sum'
 * '<S44>'  : 'pt_controller/Computer/PID Controller1/Tsamp - Integral'
 * '<S45>'  : 'pt_controller/Computer/PID Controller1/Tsamp - Ngain'
 * '<S46>'  : 'pt_controller/Computer/PID Controller1/postSat Signal'
 * '<S47>'  : 'pt_controller/Computer/PID Controller1/preSat Signal'
 * '<S48>'  : 'pt_controller/Computer/PID Controller1/Anti-windup/Disabled'
 * '<S49>'  : 'pt_controller/Computer/PID Controller1/D Gain/Disabled'
 * '<S50>'  : 'pt_controller/Computer/PID Controller1/Filter/Disabled'
 * '<S51>'  : 'pt_controller/Computer/PID Controller1/Filter ICs/Disabled'
 * '<S52>'  : 'pt_controller/Computer/PID Controller1/I Gain/Disabled'
 * '<S53>'  : 'pt_controller/Computer/PID Controller1/Ideal P Gain/Passthrough'
 * '<S54>'  : 'pt_controller/Computer/PID Controller1/Ideal P Gain Fdbk/Disabled'
 * '<S55>'  : 'pt_controller/Computer/PID Controller1/Integrator/Disabled'
 * '<S56>'  : 'pt_controller/Computer/PID Controller1/Integrator ICs/Disabled'
 * '<S57>'  : 'pt_controller/Computer/PID Controller1/N Copy/Disabled wSignal Specification'
 * '<S58>'  : 'pt_controller/Computer/PID Controller1/N Gain/Disabled'
 * '<S59>'  : 'pt_controller/Computer/PID Controller1/P Copy/Disabled'
 * '<S60>'  : 'pt_controller/Computer/PID Controller1/Parallel P Gain/External Parameters'
 * '<S61>'  : 'pt_controller/Computer/PID Controller1/Reset Signal/Disabled'
 * '<S62>'  : 'pt_controller/Computer/PID Controller1/Saturation/Enabled'
 * '<S63>'  : 'pt_controller/Computer/PID Controller1/Saturation Fdbk/Disabled'
 * '<S64>'  : 'pt_controller/Computer/PID Controller1/Sum/Passthrough_P'
 * '<S65>'  : 'pt_controller/Computer/PID Controller1/Sum Fdbk/Disabled'
 * '<S66>'  : 'pt_controller/Computer/PID Controller1/Tracking Mode/Disabled'
 * '<S67>'  : 'pt_controller/Computer/PID Controller1/Tracking Mode Sum/Passthrough'
 * '<S68>'  : 'pt_controller/Computer/PID Controller1/Tsamp - Integral/Disabled wSignal Specification'
 * '<S69>'  : 'pt_controller/Computer/PID Controller1/Tsamp - Ngain/Passthrough'
 * '<S70>'  : 'pt_controller/Computer/PID Controller1/postSat Signal/Forward_Path'
 * '<S71>'  : 'pt_controller/Computer/PID Controller1/preSat Signal/Forward_Path'
 * '<S72>'  : 'pt_controller/Computer/Rate Limiter Dynamic/Saturation Dynamic'
 * '<S73>'  : 'pt_controller/Computer/Subsystem1/Subsystem1'
 * '<S74>'  : 'pt_controller/Computer/Subsystem1/Subsystem4'
 * '<S75>'  : 'pt_controller/Computer/Subsystem2/Downforce'
 * '<S76>'  : 'pt_controller/Computer/Subsystem2/Inertial Force '
 * '<S77>'  : 'pt_controller/Computer/Subsystem2/Rolling Force'
 * '<S78>'  : 'pt_controller/Computer/Subsystem2/Subsystem1'
 * '<S79>'  : 'pt_controller/Computer/Subsystem5/Subsystem'
 * '<S80>'  : 'pt_controller/Computer/Subsystem5/Subsystem1'
 * '<S81>'  : 'pt_controller/Computer/Subsystem5/Subsystem/Subsystem'
 * '<S82>'  : 'pt_controller/Computer/Subsystem5/Subsystem/Subsystem1'
 * '<S83>'  : 'pt_controller/Computer/Subsystem5/Subsystem/Subsystem/Saturation Dynamic'
 * '<S84>'  : 'pt_controller/Computer/Subsystem5/Subsystem1/Saturation Dynamic'
 * '<S85>'  : 'pt_controller/Computer/Transmission Shift Logic/downshift_map'
 * '<S86>'  : 'pt_controller/Computer/Transmission Shift Logic/upshift_map'
 * '<S87>'  : 'pt_controller/Raptor/Blank Message'
 * '<S88>'  : 'pt_controller/Raptor/Blank Message1'
 * '<S89>'  : 'pt_controller/Raptor/Blank Message2'
 * '<S90>'  : 'pt_controller/Raptor/Publish'
 * '<S91>'  : 'pt_controller/Raptor/Publish1'
 * '<S92>'  : 'pt_controller/Raptor/Publish2'
 * '<S93>'  : 'pt_controller/Subscribe/Enabled Subsystem'
 * '<S94>'  : 'pt_controller/Subscribe1/Enabled Subsystem'
 * '<S95>'  : 'pt_controller/Subscribe10/Enabled Subsystem'
 * '<S96>'  : 'pt_controller/Subscribe11/Enabled Subsystem'
 * '<S97>'  : 'pt_controller/Subscribe12/Enabled Subsystem'
 * '<S98>'  : 'pt_controller/Subscribe13/Enabled Subsystem'
 * '<S99>'  : 'pt_controller/Subscribe3/Enabled Subsystem'
 * '<S100>' : 'pt_controller/Subscribe5/Enabled Subsystem'
 * '<S101>' : 'pt_controller/Subscribe6/Enabled Subsystem'
 * '<S102>' : 'pt_controller/Subscribe7/Enabled Subsystem'
 * '<S103>' : 'pt_controller/Subscribe8/Enabled Subsystem'
 * '<S104>' : 'pt_controller/Subscribe9/Enabled Subsystem'
 */
#endif                                 /* RTW_HEADER_pt_controller_h_ */
