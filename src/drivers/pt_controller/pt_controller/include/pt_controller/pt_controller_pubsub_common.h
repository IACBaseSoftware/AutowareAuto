// Copyright 2019 The MathWorks, Inc.
// Generated 13-Apr-2021 23:22:18
#ifndef _PT_CONTROLLER_PUBSUB_COMMON_
#define _PT_CONTROLLER_PUBSUB_COMMON_
#include "pt_controller_types.h"
#ifndef SET_QOS_VALUES
#define SET_QOS_VALUES(qosStruct, hist, dep, dur, rel)  \
    {                                                   \
        qosStruct.history = hist;                       \
        qosStruct.depth = dep;                          \
        qosStruct.durability = dur;                     \
        qosStruct.reliability = rel;                    \
    }
#endif
namespace ros2 {
    namespace matlab {
        // pt_controller/Raptor/Publish
        extern void create_Pub_pt_controller_1693(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern void publish_Pub_pt_controller_1693(const SL_Bus_std_msgs_Float32* inBus);
        // pt_controller/Raptor/Publish1
        extern void create_Pub_pt_controller_1694(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern void publish_Pub_pt_controller_1694(const SL_Bus_std_msgs_Float32* inBus);
        // pt_controller/Raptor/Publish2
        extern void create_Pub_pt_controller_1955(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern void publish_Pub_pt_controller_1955(const SL_Bus_std_msgs_UInt8* inBus);
        // pt_controller/Subscribe
        extern void create_Sub_pt_controller_1695(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_1695(SL_Bus_std_msgs_Float64* outBus);
        // pt_controller/Subscribe1
        extern void create_Sub_pt_controller_1696(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_1696(SL_Bus_std_msgs_Float64* outBus);
        // pt_controller/Subscribe10
        extern void create_Sub_pt_controller_2594(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2594(SL_Bus_std_msgs_Float32* outBus);
        // pt_controller/Subscribe11
        extern void create_Sub_pt_controller_2595(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2595(SL_Bus_std_msgs_Float32* outBus);
        // pt_controller/Subscribe12
        extern void create_Sub_pt_controller_2596(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2596(SL_Bus_std_msgs_Float32* outBus);
        // pt_controller/Subscribe13
        extern void create_Sub_pt_controller_2597(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2597(SL_Bus_std_msgs_Float32* outBus);
        // pt_controller/Subscribe3
        extern void create_Sub_pt_controller_1698(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_1698(SL_Bus_std_msgs_Float64* outBus);
        // pt_controller/Subscribe5
        extern void create_Sub_pt_controller_2589(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2589(SL_Bus_std_msgs_Float32* outBus);
        // pt_controller/Subscribe6
        extern void create_Sub_pt_controller_2590(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2590(SL_Bus_std_msgs_Float32* outBus);
        // pt_controller/Subscribe7
        extern void create_Sub_pt_controller_2591(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2591(SL_Bus_std_msgs_Float32* outBus);
        // pt_controller/Subscribe8
        extern void create_Sub_pt_controller_2592(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2592(SL_Bus_std_msgs_Float32* outBus);
        // pt_controller/Subscribe9
        extern void create_Sub_pt_controller_2593(const char *topicName, const rmw_qos_profile_t& qosProfile);
        extern bool getLatestMessage_Sub_pt_controller_2593(SL_Bus_std_msgs_Float32* outBus);
    }
}
#endif // _PT_CONTROLLER_PUBSUB_COMMON_
