// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include "replay_planner/replay_planner_node.hpp"

#include <common/types.hpp>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/int32.hpp>
#include <std_msgs/msg/string.hpp>

#include <memory>
#include <string>
#include <utility>

#include "replay_planner/replay_planner.hpp"

using std::placeholders::_1;
using autoware::common::types::float32_t;
using autoware::common::types::float64_t;


namespace autoware
{
namespace replay_planner
{

ReplayPlannerNode::ReplayPlannerNode(const rclcpp::NodeOptions & options)
:  Node{"replay_trajectory", options}
{
  const auto ct_report_topic = "ct_report";
  const auto ego_topic = "vehicle_state";
  const auto trajectory_topic = "planned_trajectory";
  float64_t min_record_distance = declare_parameter("min_record_distance").get<float64_t>();
  float64_t heading_weight = declare_parameter("heading_weight").get<float64_t>();
  std::string path_pit_exit = declare_parameter("path_pit_exit").get<std::string>();
  std::string path_oval = declare_parameter("path_oval").get<std::string>();
  std::string path_pit_entry = declare_parameter("path_pit_entry").get<std::string>();
  init(
    ego_topic, trajectory_topic, ct_report_topic, heading_weight, min_record_distance,
    path_pit_exit, path_oval, path_pit_entry);
  RCLCPP_INFO(get_logger(), "Node created");
}

void ReplayPlannerNode::init(
  const std::string & ego_topic,
  const std::string & trajectory_topic,
  const std::string & ct_report_topic,
  const float64_t heading_weight,
  const float64_t min_record_distance,
  const std::string path_pit_exit,
  const std::string path_oval,
  const std::string path_pit_entry)
{
  using rclcpp::QoS;

  // Set up subscribers for the actual recording
  using SubAllocT = rclcpp::SubscriptionOptionsWithAllocator<std::allocator<void>>;
  m_ego_sub = create_subscription<State>(
    ego_topic, QoS{10},
    [this](const State::SharedPtr msg) {on_ego(msg);}, SubAllocT{});

  // Set up publishers
  using PubAllocT = rclcpp::PublisherOptionsWithAllocator<std::allocator<void>>;
  m_trajectory_pub =
    create_publisher<Trajectory>(trajectory_topic, QoS{10}, PubAllocT{});

  printf("init created\n");
  sub =
    this->create_subscription<std_msgs::msg::Int32>(
    "in_num", 10,
    std::bind(&ReplayPlannerNode::in_num_callback, this, _1));
  pub = this->create_publisher<std_msgs::msg::Int32>("out_num", 10);

  received_ct_state = this->create_subscription<deep_orange_msgs::msg::CtReport>(
    ct_report_topic,
    10,
    std::bind(&ReplayPlannerNode::ct_state_callback, this, _1));

  // Create and set a planner object that we'll talk to
  m_replay = std::make_unique<autoware::replay_planner::ReplayPlanner>();
  m_replay->set_heading_weight(heading_weight);
  m_replay->set_min_record_distance(min_record_distance);
  m_replay->readTrajectoryBufferFromFile(path);
  m_replay->set_min_record_distance(min_record_distance);

  m_replay_pit_exit = std::make_unique<autoware::replay_planner::ReplayPlanner>();
  m_replay_pit_exit->set_heading_weight(heading_weight);
  m_replay_pit_exit->set_min_record_distance(min_record_distance);
  m_replay_pit_exit->readTrajectoryBufferFromFile(path_pit_exit);
  m_replay_pit_exit->set_min_record_distance(min_record_distance);

  m_replay_oval = std::make_unique<autoware::replay_planner::ReplayPlanner>();
  m_replay_oval->set_heading_weight(heading_weight);
  m_replay_oval->set_min_record_distance(min_record_distance);
  m_replay_oval->readTrajectoryBufferFromFile(path_oval);
  m_replay_oval->set_min_record_distance(min_record_distance);

  m_replay_pit_entry = std::make_unique<autoware::replay_planner::ReplayPlanner>();
  m_replay_pit_entry->set_heading_weight(heading_weight);
  m_replay_pit_entry->set_min_record_distance(min_record_distance);
  m_replay_pit_entry->readTrajectoryBufferFromFile(path_pit_entry);
  m_replay_pit_entry->set_min_record_distance(min_record_distance);
}

void ReplayPlannerNode::on_ego(const State::SharedPtr & msg)
{
  Trajectory traj;
  // std::cout<<"Requested mode "<<requested_mode<<std::endl;
  switch (requested_mode) {
    case 1:
      m_replay_pit_exit->set_mode(requested_mode);
      m_replay_pit_exit->set_speed(5.0);
      // RCLCPP_INFO(this->get_logger(), "pit exit");
       RCLCPP_DEBUG_ONCE(this->get_logger(),"pit exit");
      traj = m_replay_pit_exit->plan(*msg);
      break;

    case 2:
      m_replay_oval->set_mode(requested_mode);
      m_replay_oval->set_speed(15.0);
      // RCLCPP_INFO(this->get_logger(), "oval");
      RCLCPP_DEBUG_ONCE(this->get_logger(),"oval");
      traj = m_replay_oval->plan(*msg);
      break;

    case 3:
      m_replay_pit_entry->set_mode(requested_mode);
      m_replay_pit_entry->set_speed(5.0);
      // RCLCPP_INFO(this->get_logger(), "pit entry");
      RCLCPP_DEBUG_ONCE(this->get_logger(),"pit entry");
      traj = m_replay_pit_entry->plan(*msg);
      break;

    default:
      // RCLCPP_INFO(this->get_logger(), "pit \n");
      RCLCPP_DEBUG_ONCE(this->get_logger(),"pit");
      break;
  }
  m_trajectory_pub->publish(traj);
}


void ReplayPlannerNode::in_num_callback(const std_msgs::msg::Int32::SharedPtr msg) const
{
  // RCLCPP_INFO(this->get_logger(),"number is %d\n",(msg->data));
  requested_mode = msg->data;
  m_replay->set_mode(requested_mode);
  auto rep_msg = std_msgs::msg::Int32();
  rep_msg.data = m_replay->a;
  pub->publish(rep_msg);
}

void ReplayPlannerNode::ct_state_callback(const CtReport::SharedPtr msg) const
{
  // RCLCPP_INFO(this->get_logger(),"number is %d\n",(msg->data));
  // requested_mode = msg->ct_state;
  if (msg->ct_state == 8) {  // 8 - CAUTION mode
    if (m_replay->get_mode() == 0) {
      requested_mode = 1;
    } else if (m_replay->get_mode() == 2) {
      requested_mode = 3;
    }
  } else if (msg->ct_state == 9) {  // 9 - NOM_RACE mode
    requested_mode = 2;
  }
  m_replay->set_mode(requested_mode);
  auto rep_msg = std_msgs::msg::Int32();
  rep_msg.data = m_replay->a;
  pub->publish(rep_msg);
}

}  // namespace replay_planner
}  // namespace autoware
