# Copyright 2020 The Autoware Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
cmake_minimum_required(VERSION 3.5)

project(replay_planner)

# dependencies
find_package(ament_cmake_auto REQUIRED)
ament_auto_find_build_dependencies()
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)

set(REPLAY_PLANNER_LIB_SRC
  src/replay_planner.cpp
)

set(REPLAY_PLANNER_LIB_HEADERS
  include/replay_planner/replay_planner.hpp
  include/replay_planner/visibility_control.hpp
)

# generate library
ament_auto_add_library(${PROJECT_NAME} SHARED
  ${REPLAY_PLANNER_LIB_SRC}
  ${REPLAY_PLANNER_LIB_HEADERS}
)
autoware_set_compile_options(${PROJECT_NAME})

set(REPLAY_PLANNER_NODE_SRC
  src/replay_planner_node.cpp
)

set(REPLAY_PLANNER_NODE_HEADERS
  include/replay_planner/replay_planner_node.hpp
)

# generate component node library
ament_auto_add_library(${PROJECT_NAME}_node SHARED
  ${REPLAY_PLANNER_NODE_SRC}
  ${REPLAY_PLANNER_NODE_HEADERS}
)
autoware_set_compile_options(${PROJECT_NAME}_node)
rclcpp_components_register_nodes(${PROJECT_NAME}_node "autoware::replay_planner::ReplayPlannerNode")

set(REPLAY_PLANNER_EXE_SRC
  src/main.cpp
)

# generate stand-alone executable
ament_auto_add_executable(${PROJECT_NAME}_exe
  ${REPLAY_PLANNER_EXE_SRC}
)
autoware_set_compile_options(${PROJECT_NAME}_exe)

# Testing
if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  ament_lint_auto_find_test_dependencies()
  find_package(rclcpp REQUIRED)
  find_package(std_msgs REQUIRED)
endif()

# ament package generation and installing
ament_auto_package(
  INSTALL_TO_SHARE
    launch
    param
)
