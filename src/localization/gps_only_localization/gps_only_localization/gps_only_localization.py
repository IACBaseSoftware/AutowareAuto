#! usr/bin/env python3

###
##referenced from https://gist.github.com/sbarratt/a72bede917b482826192bf34f9ff5d0b
###

import rclpy
from rclpy.node import Node
from novatel_gps_msgs.msg import Inspva, NovatelCorrectedImuData, NovatelVelocity
import numpy as np
import math
from deep_orange_msgs.msg import Coordinates
from autoware_auto_msgs.msg import VehicleKinematicState
from raptor_dbw_msgs.msg import SteeringReport
from sensor_msgs.msg import NavSatFix

class GpsOnlyLocalization(Node):


    latitude = 0 
    longitude = 0
    height = 0
    heading = 0
    # east = 0, north = 0, up = 0
    global position

    def __init__(self):
        super().__init__("GPS_only_localization")
        self.inspva_subscriber = self.create_subscription(Inspva,"/inspva",self.inspva_callback,100)

        #uncomment while testing with lgsvl
        # self.inspva_subscriber = self.create_subscription(NavSatFix,"/gnss/fix",self.inspva_callback,100)
                       
        self.corrimudata_subscriber = self.create_subscription(NovatelCorrectedImuData,"/corrimudata",self.corrimudata_callback,100)
        # self.bestvel_subscriber = self.create_subscription(NovatelVelocity,"/bestvel",self.bestvel_callback,100)
        self.steering_report_subscriber = self.create_subscription(SteeringReport,"/raptor_dbw_interface/steering_report",self.steering_report_callback,100)
        self.state_publisher = self.create_publisher(VehicleKinematicState,"/converted_data",100)


    def inspva_callback(self,data):
        # lat0 = 34.81534876886       #lla of the origin point in the local map #Jtekt
        # lon0 = -82.32689845604
        # h0 = 249.1833 

        lat0 = 39.782256366777965       #lla of the origin point in the local map #juncos
        lon0 = -86.23722088158846
        h0 = 188.5784457186237 

        # lat0 = 37.380446431       #for testing with lgsvl
        # lon0 = -121.909066915
        # h0 = -19.243675231        #for testing with lgsvl

        # heading360 = 0.0
        # heading180 = 0.0
        heading_non_normalised = 0.0
        heading_normalised = 0.0

        a = 6378137.000           #WGS-84 Earth semimajor axis (m)
        b = 6356752.314245179      #Derived Earth semiminor axis (m)
        f = (a - b) / a           #Ellipsoid Flatness
        # f_inv = 1.0 / f         #Inverse flattening
        e_sq = f * (2 - f)        #Square of Eccentricity 

        ##lla2ecef conversion
        lat_rad = np.radians(data.latitude)
        lon_rad = np.radians(data.longitude)
        s = math.sin(lat_rad)
        N = a / math.sqrt(1 - e_sq* s * s)

        sin_lat = math.sin(lat_rad)
        cos_lat = math.cos(lat_rad)
        cos_lon = math.cos(lon_rad)
        sin_lon = math.sin(lon_rad)

        ecef_x = (data.height + N) * cos_lat * cos_lon          #change to altitude for testing with lgsvl
        ecef_y = (data.height + N) * cos_lat * sin_lon
        ecef_z = (data.height + (1 - e_sq) * N) * sin_lat

        ##ecef2enu conversion
        lamb = math.radians(lat0)
        phi = math.radians(lon0)
        s = math.sin(lamb)
        N = a / math.sqrt(1 - e_sq * s * s)

        sin_lambda = math.sin(lamb)
        cos_lambda = math.cos(lamb)
        sin_phi = math.sin(phi)
        cos_phi = math.cos(phi)

        x0 = (h0 + N) * cos_lambda * cos_phi
        y0 = (h0 + N) * cos_lambda * sin_phi
        z0 = (h0 + (1 - e_sq) * N) * sin_lambda

        xd = ecef_x - x0
        yd = ecef_y - y0
        zd = ecef_z - z0

        position.state.x = -sin_phi * xd + cos_phi * yd #east
        position.state.y = -cos_phi * sin_lambda * xd - sin_lambda * sin_phi * yd + cos_lambda * zd #north
        # position.state.z = cos_lambda * cos_phi * xd + cos_lambda * sin_phi * yd + sin_lambda * zd #up ##not used anywhere
   
        heading_non_normalised = math.radians(90 - data.azimuth)
        heading_normalised = math.atan2(math.sin(heading_non_normalised),math.cos(heading_non_normalised))  

        position.state.longitudinal_velocity_mps = data.east_velocity*math.cos(heading_normalised) + data.north_velocity*math.(heading_normalised)
        
        position.header.frame_id = 'map'
        position.header.stamp = self.get_clock().now().to_msg()
        position.state.heading.imag = math.sin(heading_normalised/2.0)
        position.state.heading.real = math.cos(heading_normalised/2.0) 
        self.state_publisher.publish(position)

    def corrimudata_callback(self,data):
        position.state.heading_rate_rps = data.yaw_rate
        position.state.acceleration_mps2 = data.longitudinal_acceleration
        self.state_publisher.publish(position)

    # def bestvel_callback(self,data):
    #     position.state.longitudinal_velocity_mps = data.horizontal_speed
    #     self.state_publisher.publish(position)

    def steering_report_callback(self,data):
        steering_gear_ratio = 9
        ground_wheel_angle = math.radians(data.steering_wheel_angle)/steering_gear_ratio
        position.state.front_wheel_angle_rad = ground_wheel_angle
        self.state_publisher.publish(position)

def main(args=None):
    rclpy.init()
    global position
    position = VehicleKinematicState()
    gps_node = GpsOnlyLocalization()
    rclpy.spin(gps_node)
    rclpy.shutdown()
    
if __name__  == '__main__':
    main()
