"""Launch file for GPS Only Localization"""

from ament_index_python import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.actions import Node
import os


def get_share_file(package_name, file_name):
    return os.path.join(get_package_share_directory(package_name), file_name)

def generate_launch_description():


     # --------------------------------- Params -------------------------------

    # static_publisher_param_file = get_share_file(
    #     package_name='gps_only_localization', file_name='static_publisher_param_file.param.yaml')

     # --------------------------------- Arguments -------------------------------

    # static_publisher_param_file = DeclareLaunchArgument(
    #     'static_publisher_param_file',
    #     default_value=static_publisher_param_file,
    #     description='Path to Param file',
    # )

     # -------------------------------- Nodes-----------------------------------

    gps_localization = Node(
        package='gps_only_localization',
        executable='gps_only_localization',
        name= 'gps_only_localization',
        node_namespace='',
    )

    static_transform = Node(
        package='tf2_ros',
        executable='static_transform_publisher',
        name='static_transform_publisher',
        # parameters=[LaunchConfiguration('static_publisher_param_file')],
        arguments=['0.1593001', '0', '0.2533325', '0', '0', '180', '0', 'base_link', 'gnss'],
    )

    measurement_transformer_nodes_launch_file_path = get_share_file(
        package_name='measurement_transformer_nodes',
        file_name='launch/vks_child_frame_transformer_node.launch.py')
    measurement_transformer = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(measurement_transformer_nodes_launch_file_path)
    )

    return LaunchDescription([
        gps_localization,
        # static_publisher_param_file,
        static_transform,
        measurement_transformer,
    ])